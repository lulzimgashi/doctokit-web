import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Switch, Redirect } from 'react-router-dom'

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Dashboard from './../screens/Dashboard';
import Users from './../screens/Users';
import Services from './../screens/Services';
import Settings from './../screens/Settings';

import Sidebar from './../components/Siderbar';
import Confirm from '../components/ConfirmDialog';
import NavBar from '../components/Navbar';
import Appointments from '../screens/Appointments';
import Patients from '../screens/Patients';
import Patient from '../screens/Patient';


class ProtectedRoutes extends Component {
    state = { sidebarOn: false }

    switch = () => {
        this.setState({ sidebarOn: !this.state.sidebarOn });
    }

    render() {
        if (!this.props.user) {
            return <Redirect to='/login' />
        }
        return (
            <div style={{ display: 'flex', width: '100%', height: '100%', overflowX: 'hidden' }}>
                <Sidebar history={this.props.history} />
                <div className={'not-full-width-height z-index-1 ' + (this.state.sidebarOn ? 'on' : 'off')}>
                    <NavBar switch={this.switch} />
                    <Switch>
                        <Route exact path='/appointments' component={Appointments} />
                        <Route exact path='/patients' component={Patients} />
                        <Route exact path='/patients/:id' component={Patient} />
                        <Route exact path='/services' component={Services} />
                        <Route exact path='/settings' component={Settings} />
                        <Route component={Dashboard} />
                    </Switch>
                </div>
                <Confirm />
                <ToastContainer />
            </div>)
    }
}


const mapStateToProps = state => {
    return { loading: state.loading, user: state.user }
}

export default connect(mapStateToProps)(ProtectedRoutes);
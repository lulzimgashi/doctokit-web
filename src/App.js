import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import axios from 'axios';
//screens
import Login from './screens/Login';
import Reset from './screens/ResetPassword';
import Register from './screens/Register';
import ProtectedRoutes from './configuration/ProtectedRoutes';

class App extends Component {


  componentWillMount() {
    axios.interceptors.request.use(function (config) {
      config.auth = {
        username: '2',
        password: '2'
      }
      return config;
    }, function (error) {
      return Promise.reject(error);
    });

  }

  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path='/login' component={Login} />
          <Route exact path='/register' component={Register} />
          <Route exact path='/reset' component={Reset} />
          <Route component={ProtectedRoutes} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;


import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
//action
import { reset } from '../redux/actions'
//components
import Input from '../components/Input'
import Button from '../components/Button'

import '../assets/css/login.css';

class Login extends Component {
    state = { loading: false, error: '', success: '' }
    reset = () => {
        if (!this.email.state.error) {
            this.setState({ loading: true });
            reset({}, (error, value) => {
                if (error) {
                    this.setState({ error: value, success: '', loading: false })
                } else {
                    this.setState({ error: '', success: value, loading: false })
                }
            })
        }
    }

    login = () => {
        if (!this.state.loading) {
            this.props.history.push('/login');
        }
    }
    render() {
        if (this.props.user) {
            return <Redirect to='/' />
        }
        return (
            <div className='login-main-container bg-color-lightgrey'>
                <div className='login-form'>

                    <div className='login-form__item login-form__item--first'>
                        <span className='default-title login-form__title'>Welcome to <br /> Doctokit</span>
                        <span className='default-p mrt-50px'>Reset password using your email address. Please check your inbox or spam folder after you click Reset Button</span>
                    </div>

                    <div className='login-form__item login-form__item--last'>
                        <span className='default-title color-grey'>Forgot Password</span>

                        <div className='login-form__inputs-wrapper'>
                            <div style={{ height: 40 }}></div>
                            <Input ref={ref => this.email = ref} label placeholder='Email' type='email' className='mrb-20px' />
                            <span className='dk-form-error-txt'>{this.state.error}</span>
                            <span className='dk-form-success-txt'>{this.state.success}</span>
                            <Button title='Reset' loading={this.state.loading} click={this.reset} className='self-end' />
                        </div>


                        <div className='login-form__bottom' onClick={this.login}>
                            <i className="icon">chevron_right</i>
                            <span className='login-form__bottom-span pointer'>I have an account</span>
                        </div>
                    </div>


                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return { loading: state.loading, user: state.user }
}

export default connect(mapStateToProps)(Login);
import React, { Component } from 'react';
import { connect } from 'react-redux';
import '../assets/css/services.css';
//components
import Button from '../components/Button';
import SearchInput from '../components/SearchInput';
import ServiceTableRow from '../components/services/ServiceTableRow';
import AddServicePopup from '../components/services/AddServicePopup';

import '../assets/css/login.css';

class Services extends Component {
    state = { popup: false, items: [] }

    componentWillMount() {
        this.setState({ items: this.props.user.clinic.services });
    }

    addItem = (item) => {
        this.state.items.unshift(item);
        this.closePopup();
    }

    delete = (id) => {
        var items = this.state.items;
        for (var i in items) {
            if (items[i].id === id) {
                items.splice(i, 1);
                this.setState({ items });
                break;
            }
        }
    }

    closePopup = () => {
        this.setState({ popup: false });
    }

    showAddPopupService = () => {
        this.setState({ popup: true });
    }

    search = (value) => {
        if (value.length === 0) {
            this.setState({ items: this.props.user.clinic.services });
        } else {
            var items = this.props.user.clinic.services;
            var newItems = [];
            for (var e in items) {
                if (items[e].name.toString().toLowerCase().includes(value.toString().toLowerCase())) {
                    newItems.push(items[e]);
                }
            }
            this.setState({ items: newItems });
        }
    }

    renderRow = () => {
        return this.state.items.map((item, i) => {
            const renderPopupBottom = i >= this.state.items.length - 2 ? true : false;
            return <ServiceTableRow key={item.id} item={item} dispatch={this.props.dispatch} history={this.props.history} delete={this.delete} renderPopupBottom={renderPopupBottom} />
        })
    }

    render() {
        return (
            <div className='main-container main-container--bg-grey'>
                <div className='main-container__header'>
                    <span className='main-container__title'>Services</span>

                    <Button click={this.showAddPopupService} title='Add Service' className='dk-btn--medium-height mrl-auto' />
                </div>

                <SearchInput icon onChange={this.search} placeholder='Search Services' className='mrt-20px dk-input-n-icon--patients' ref={ref => this.searchInput = ref} />


                <div className='table-wrapper mrt-20px'>
                    <div className='table-header-wrapper'>
                        <span className='flex-1 table-margin'>Service</span>
                        <span className='min-width-110px table-margin'>Status</span>
                        <span className='min-width-70px table-margin'></span>
                    </div>

                    {this.renderRow()}
                </div>

                {this.state.popup && <AddServicePopup dispatch={this.props.dispatch} parent={this} />}
            </div>

        );
    }
}

const mapStateToProps = state => {
    return { loading: state.loading, user: state.user }
}

export default connect(mapStateToProps)(Services);
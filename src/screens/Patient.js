import React, { Component } from 'react';

//components
import PatientInformationBox from '../components/patients/PatientInformationBox';
import MessageBox from '../components/patients/MessageBox';
import Tabs from '../components/patients/Tabs';

class Patient extends Component {
    render() {
        return (
            <div className='main-container main-container--bg-grey'>
                <div className='p-boxes'>
                    <PatientInformationBox />
                    <MessageBox />
                </div>

                <Tabs />
            </div>
        );
    }
}

export default Patient;
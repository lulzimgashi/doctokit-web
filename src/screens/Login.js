import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
//actions
import { login } from '../redux/actions';

//components
import Input from '../components/Input'
import Button from '../components/Button'

import '../assets/css/login.css';


class Login extends Component {
    state = { loading: false, error: '' }
    login = () => {
        if (!this.email.state.error) {
            this.setState({ loading: true });
            const user = { email: this.email.input.value, password: this.password.input.value };

            this.props.dispatch(login(user, (error) => {
                if (error) {
                    this.setState({ loading: false, error });
                }
            }))
        }
    }

    resetPassword = () => {
        if (!this.state.loading) {
            this.props.history.push('/reset');
        }
    }

    render() {
        if (this.props.user) {
            return <Redirect to='/' />
        }
        return (
            <div className='login-main-container'>
                <div className='login-form'>

                    <div className='login-form__item login-form__item--first'>
                        <span className='default-title login-form__title'>Welcome to <br /> Doctokit</span>
                        <span className='default-p mrt-50px' >Login using your email address and password</span>
                    </div>

                    <div className='login-form__item login-form__item--last'>
                        <span className='default-title color-grey'>Login</span>

                        <div className='login-form__inputs-wrapper' >
                            <Input ref={ref => this.email = ref} label placeholder='Email' type='email' className='mrb-15px' />
                            <Input ref={ref => this.password = ref} label placeholder='Password' type='password' className='mrb-20px' />
                            <span className='dk-form-error-txt'>{this.state.error}</span>
                            <Button title='Sign in' loading={this.state.loading} click={this.login} className='self-end' />
                        </div>


                        <div className='login-form__bottom' onClick={this.resetPassword}>
                            <i className="icon">chevron_right</i>
                            <span className='login-form__bottom-span pointer'>Forgot Password</span>
                        </div>
                    </div>


                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return { loading: state.loading, user: state.user }
}

export default connect(mapStateToProps)(Login);
import React, { Component } from 'react';
//components
import TabHeader from '../components/TabHeader';
import TabHeaderItem from '../components/TabHeaderItem';
//tabs
import ProfileTab from '../components/settings/ProfileTab';
import OptionsTab from '../components/settings/OptionsTab';

import '../assets/css/settings.css';

class Settings extends Component {
    state = { tab: 0 }

    switchTab = (index) => {
        if (index !== this.state.tab) {
            this.setState({ tab: index });
        }
    }

    render() {
        return (
                <div className='main-container main-container--bg-grey'>

                    <div className='main-container__header'>
                        <span className='main-container__title'>Settings</span>
                    </div>

                    <TabHeader className='mrt-25px'>
                        <TabHeaderItem icon='face' name='Profile' onClick={this.switchTab} index={0} active={this.state.tab === 0} />
                        <TabHeaderItem icon='tune' name='Options' onClick={this.switchTab} index={1} active={this.state.tab === 1} />
                    </TabHeader>

                    <ProfileTab index={0} active={this.state.tab === 0}/>
                    <OptionsTab index={1} active={this.state.tab === 1}/>

                </div>
        );
    }
}

export default Settings;
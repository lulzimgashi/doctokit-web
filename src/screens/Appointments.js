import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
//actions
import { getAppointmentsForDate } from '../redux/actions';

// components
import DoctorSelect from '../components/appointments/DoctorSelect';
import DatePicker from '../components/appointments/DatePicker';
import ViewAppointmentPopup from '../components/appointments/ViewAppointmentPopup';
import AddUserPopup from '../components/appointments/AddUserPopup';
import SearchUserPopup from '../components/appointments/SearchUserPopup';
import Calendar from '../components/appointments/Calendar';
import Loader from '../components/Loader';

import '../assets/css/appointments.css';



class Appointments extends Component {
    state = { appointment: null, addPopupMode: 0, tempEvent: null, loading: true, doctors: [], selectedDoctor: 0 }

    componentDidMount() {

        this.getAppointmentForDate();
    }

    getAppointmentForDate = (date) => {
        date = date ? date.format('YYYY-MM-DD') : moment().format('YYYY-MM-DD');

        this.props.dispatch(getAppointmentsForDate(date, (error, data) => {
            if (error) {
                this.setState({ loading: false });
            } else {
                this.setState({ doctors: data, loading: false });
            }
        }));
    }

    doctorChanged = (item, i) => {
        this.setState({ selectedDoctor: i })
    }

    dateChanged = (date) => {
        this.setState({ loading: true });
        this.getAppointmentForDate(date);
        this.calendar.goToDate(date);
    }

    eventClicked = (event) => {
        this.setState({ appointment: event });
    }

    eventSelected = (event) => {
        this.setState({ addPopupMode: 2, tempEvent: event });
    }

    switchEventPopup = (mode) => {
        this.setState({ addPopupMode: mode })
    }

    eventAddedSuccesfully = (event) => {
        this.state.doctors[this.state.selectedDoctor].appointments.push(event);
        this.setState({ tempEvent: null, addPopupMode: null })
    }

    eventDeleted = () => {
        var appointments = this.state.doctors[this.state.selectedDoctor].appointments;

        for (var e in appointments) {
            if (appointments[e].id === this.state.appointment.id) {
                appointments.splice(e, 1);
                break;
            }
        }

        this.setState({ appointment: null });
    }

    render() {
        const { addPopupMode, appointment, loading, doctors, selectedDoctor } = this.state;


        return (
            <div className='main-container main-container--bg-grey'>
                <div className='main-container__header'>
                    <span className='main-container__title'>Appointments</span>
                    {loading && <Loader color='#456AEB' className='mrl-10px' />}
                </div>

                <div className='appointments-header'>
                    <DoctorSelect ref={ref => this.select = ref}
                        items={doctors}
                        selected={selectedDoctor}
                        onChange={this.doctorChanged} />

                    <DatePicker
                        label='Calendar Date'
                        onChange={this.dateChanged}
                        right />
                </div>

                <Calendar ref={ref => this.calendar = ref} parent={this} doctor={doctors[selectedDoctor]} dispatch={this.props.dispatch} loading={this.state.loading} />
                <div style={{ width: '100%', minHeight: 50 }} />


                {appointment && <ViewAppointmentPopup parent={this} close={() => this.setState({ appointment: null })} appointment={this.state.appointment} />}
                {addPopupMode === 1 && <AddUserPopup event={this.state.tempEvent} parent={this} close={() => this.setState({ addPopupMode: 0, tempEvent: null })} doctor={doctors[selectedDoctor]} />}
                {addPopupMode === 2 && <SearchUserPopup event={this.state.tempEvent} parent={this} close={() => this.setState({ addPopupMode: 0, tempEvent: null })} doctor={doctors[selectedDoctor]} />}

            </div>
        );
    }
}



const mapStateToProps = state => {
    return { user: state.user }
}

export default connect(mapStateToProps)(Appointments);
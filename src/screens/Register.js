import React, { Component } from 'react';
import { connect } from 'react-redux';
import {  Redirect } from 'react-router-dom'

class Register extends Component {
    render() {
        if(this.props.user){
            return <Redirect to='/'/>
        }
        
        return (
            <div>
                Register
            </div>
        );
    }
}

const mapStateToProps=state=>{
    return {loading:state.loading,user:state.user}
}

export default connect(mapStateToProps)(Register);
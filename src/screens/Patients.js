import React, { Component } from 'react';
import { connect } from 'react-redux'
import _ from 'underscore'
//components
import Loader from '../components/Loader';
import SearchInput from '../components/SearchInput';
import Button from '../components/Button';
import OrderBy from '../components/patients/OrderBy';
import AddPatientPopup from '../components/patients/AddPatientPopup';
//actions
import { searchPatients } from '../redux/actions';
//css
import '../assets/css/patients.css';

class Patients extends Component {
    state = { loading: false, addPopup: false, patients: [] }

    componentDidMount() {
        this.setState({ loading: true });
        this.props.dispatch(searchPatients('', 100, (error, data) => {
            if (error) {
                this.setState({ loading: false });
            } else {
                this.setState({ loading: false, patients: data });
            }
        }));
    }


    search = () => {
        if (!this.state.loading && this.searchInput.input.value.length > 2) {
            this.setState({ loading: true });

            this.props.dispatch(searchPatients(this.searchInput.input.value, 100, (error, data) => {
                if (error) {
                    this.setState({ loading: false });
                } else {
                    this.setState({ loading: false, patients: data });
                }
            }));
        }
    }

    orderBy = (name, reverse) => {
        var sortedPatiens = _.sortBy(this.state.patients, name);


        this.setState({ patients: sortedPatiens });
    }


    renderRows = () => {
        return this.state.patients.map((patient, i) => {
            return (
                <div key={patient.id} onClick={() => this.rowClicked(patient)}>
                    <div className='table-row-wrapper table-row-wrapper--border-bottom table-row-wrapper--hover pointer'>
                        <span className='flex-1 min-width-200px table-margin name'>{patient.firstName + ' ' + patient.lastName}</span>
                        <span className='flex-1 min-width-50px table-margin'>{patient.gender}</span>
                        <span className='flex-1 min-width-70px table-margin'>{patient.birthDate}</span>
                        <span className='flex-1 min-width-110px table-margin'>{patient.phone}</span>
                        <span className='flex-1 min-width-170px table-margin'>{patient.email}</span>
                        <span className='flex-1 min-width-170px table-margin'>{patient.address}</span>
                        <span className='min-width-50px table-margin more'>More <i className='material-icons'>chevron_right</i> </span>
                    </div>
                </div>);
        })
    }

    rowClicked = (patient) => {
        this.props.history.push('patients/' + patient.id);
    }

    openAddPopup = () => {
        this.setState({ addPopup: true });
    }

    closeAddPopup = () => {
        this.setState({ addPopup: false })
    }

    searchOnChange = () => {

    }

    render() {
        const { loading, addPopup } = this.state;

        return (
            <div className='main-container main-container--bg-grey'>
                <div className='main-container__header'>
                    <span className='main-container__title'>Patients</span>
                    {loading && <Loader color='#456AEB' className='mrl-10px' />}

                    <Button click={this.openAddPopup} title='Add Patient' className='dk-btn--medium-height mrl-auto' />
                </div>


                <div className='patients-search-w'>
                    <div className='patients-search-w__first'>
                        <SearchInput className='dk-input-n-icon--patients' onChange={this.searchOnChange} placeholder='Search Patients' ref={ref => this.searchInput = ref} />
                        <Button click={this.search} icon='search' className='dk-btn--color-normal dk-btn--search-patients' />
                    </div>

                    <OrderBy orderBy={this.orderBy} />
                </div>


                <div className='table-wrapper mrt-15px'>
                    <div>
                        <div className='table-header-wrapper'>
                            <span className='flex-1 min-width-200px table-margin'>Name</span>
                            <span className='flex-1 min-width-50px table-margin'>Gender</span>
                            <span className='flex-1 min-width-70px table-margin'>Birth Date</span>
                            <span className='flex-1 min-width-110px table-margin'>Phone</span>
                            <span className='flex-1 min-width-170px table-margin'>Email</span>
                            <span className='flex-1 min-width-170px table-margin'>Address</span>
                            <span className='min-width-50px table-margin'></span>
                        </div>
                    </div>
                    {this.renderRows()}
                </div>

                {addPopup && <AddPatientPopup close={this.closeAddPopup} parent={this} />}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return { user: state.user }
}

export default connect(mapStateToProps)(Patients);
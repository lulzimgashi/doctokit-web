import { LOGIN_DONE, LOGOUT_DONE, CLINIC_UPDATED } from '../actions';

const user ={"id":"4028800262c44cb60162c44fdc510001","firstName":"lulzim","lastName":"lulzim","email":"2","username":"2","password":"PROTECTED","enabled":true,"doctor":true,"clinicId":"4028800262c83a450162c83aa3af0000","phone":2,"authorities":["ROLE_ADMIN","ROLE_PATIENT"],"appointments":null,"clinic":{"id":"4028800262c83a450162c83aa3af0000","name":"asd","description":"asd","email":"clinic_email2","address":"Prishina 13002","phone":123122,"website":"www.clinic.co2","profileImage":"https://thevoicefinder.com/wp-content/themes/the-voice-finder/images/default-img.png","coverImage":"https://ak1.picdn.net/shutterstock/videos/13779911/thumb/1.jpg","language":"al","type":null,"sms":true,"enabled":false,"slotTime":25,"location":"Kosovo","longitude":12.3332,"latitude":12.3332,"days":[{"id":"402880e962eda8ed0162edaa11140006","clinicId":"4028800262c83a450162c83aa3af0000","day":7,"start":"09:00:00","end":"19:00:00","open":true},{"id":"402880e962eda8ed0162edaa11140005","clinicId":"4028800262c83a450162c83aa3af0000","day":6,"start":"09:10:00","end":"19:10:00","open":false},{"id":"402880e962eda8ed0162edaa11140004","clinicId":"4028800262c83a450162c83aa3af0000","day":5,"start":"09:20:00","end":"19:20:00","open":false},{"id":"402880e962eda8ed0162edaa11140003","clinicId":"4028800262c83a450162c83aa3af0000","day":4,"start":"09:30:00","end":"19:30:00","open":false},{"id":"402880e962eda8ed0162edaa11140002","clinicId":"4028800262c83a450162c83aa3af0000","day":3,"start":"09:40:00","end":"19:40:00","open":false},{"id":"402880e962eda8ed0162edaa11130001","clinicId":"4028800262c83a450162c83aa3af0000","day":2,"start":"09:50:00","end":"19:50:00","open":false},{"id":"402880e962eda8ed0162edaa110f0000","clinicId":"4028800262c83a450162c83aa3af0000","day":1,"start":"10:00:00","end":"20:00:00","open":false}],"services":[{"id":"402880e962ee40070162ee4206cc0000","name":"1","active":true,"clinicId":"4028800262c83a450162c83aa3af0000"},{"id":"402880eb63882cc10163882e9d800000","name":"service1","active":true,"clinicId":"4028800262c83a450162c83aa3af0000"},{"id":"402880eb63882cc10163882ea5d40001","name":"service3","active":true,"clinicId":"4028800262c83a450162c83aa3af0000"},{"id":"402880eb63882cc10163882eaf190002","name":"service4","active":true,"clinicId":"4028800262c83a450162c83aa3af0000"},{"id":"402880eb63882cc10163882eb7c80003","name":"service5","active":true,"clinicId":"4028800262c83a450162c83aa3af0000"},{"id":"402880eb63882cc10163882ec03a0004","name":"service6","active":true,"clinicId":"4028800262c83a450162c83aa3af0000"},{"id":"402880eb63882cc10163882ec9340005","name":"service7","active":true,"clinicId":"4028800262c83a450162c83aa3af0000"}]}};
// const user = null;

export default (state = user, action) => {
    switch (action.type) {
        case LOGIN_DONE:
            return action.payload;
        case LOGOUT_DONE:
            return null;
        case CLINIC_UPDATED:
            state.clinic = action.payload;
            return { ...state }
        default:
            return state;
    }
}
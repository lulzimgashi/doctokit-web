import { LOGIN_DONE } from './types';
import { API } from '../../configuration/configuration';
import axios from 'axios';


export const login = (user, callback) => dispatch => {

    axios.interceptors.request.use(function (config) {
        config.auth={
            username:user.email,
            password:user.password
        }
        return config;
    }, function (error) {
        return Promise.reject(error);
    });


    axios.get(API + 'users/login', {
        auth: {
            username: user.email,
            password: user.password
        }
    }).then(response => {
        response.data.password = user.password;
        dispatch({ type: LOGIN_DONE, payload: response.data });


    }).catch(error => {

        if (error.response.status === 401) {
            callback('Wrong Email or Password')
        } else if (error.response.status === 400) {
            callback(error.response.data);
        } else if (error.response.status === 500) {
            callback('Something went wrong !');
        }
    })
}

export const reset = (user, callback) => {
    setTimeout(() => {
        callback(false, 'Success u won')
    }, 1000)
}
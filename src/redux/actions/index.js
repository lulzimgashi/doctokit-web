import { LOGOUT_DONE } from './types';
import { toast } from 'react-toastify';

export * from './types'
export * from './user';
export * from './services'
export * from './days';
export * from './clinic';
export * from './appointment';
export * from './patient';

export const handleError = (error, dispatch, callback) => {
    
    if(!error.response){
        callback(true);
        toast.info('Something went wrong!', {
            position: "bottom-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
        });
        return;
    }

    if (error.response.status === 401) {
        dispatch({ type: LOGOUT_DONE })
        return;
    } else if (error.response.status === 400) {

        callback(true);
        toast.info(error.response.data, {
            position: "bottom-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
        });
        return;
    }
    else if (error.response.status === 403) {
        callback(true);
        toast.info('You dont have access for that', {
            position: "bottom-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
        });
        return;
    } else if (error.response.status === 500) {

        callback(true);
        toast.info('Something went wrong!', {
            position: "bottom-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
        });

        return;
    }
}
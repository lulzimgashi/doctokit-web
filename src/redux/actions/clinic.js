import axios from 'axios';
import { API } from '../../configuration/configuration';
import { handleError } from './index';
import { CLINIC_UPDATED } from './types';


export const updateClinic = (newClinic, callback) => dispatch => {

    axios.put(API + 'clinics', newClinic)
        .then(response => {
            callback();
            dispatch({type:CLINIC_UPDATED,payload:newClinic});
        }).catch(error => {
            handleError(error, dispatch, callback);
        })

}

export const updateClinicImg=(clinic,blob,type,callback)=>dispatch=>{

    const multipart = new FormData();
    multipart.append('file',blob);
    

    axios.put(API+'clinics/img?type='+type,multipart)
    .then(response=>{
        callback(false,response.data);
    }).catch(error=>{
        handleError(error, dispatch, callback);
    })
}
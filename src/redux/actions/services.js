import axios from 'axios';
import { API } from '../../configuration/configuration';
import { handleError } from './index';

export const saveService = (newService, callback) => dispatch => {

    axios.post(API + 'services', newService)
        .then(response => {
            callback(false, response.data);
        }).catch(error => {
            console.log(error);
            handleError(error, dispatch, callback);
        })

}

export const updateService = (newService, callback) => dispatch => {

    axios.put(API + 'services', newService)
        .then(response => {
            callback();
        }).catch(error => {
            handleError(error, dispatch, callback);
        })

}

export const deleteService = (id, callback) => dispatch => {

    axios.delete(API + 'services/' + id)
        .then(response => {
            callback();
        }).catch(error => {
            handleError(error, dispatch, callback);
        })

}


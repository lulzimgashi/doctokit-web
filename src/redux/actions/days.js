import axios from 'axios';
import { API } from '../../configuration/configuration';
import { handleError } from './index';

export const updateDay = (day, callback) => dispatch => {

    axios.put(API + 'clinics/day', day)
        .then(response => {
            callback();
        }).catch(error => {
            handleError(error, dispatch, callback);
        })

}
import axios from 'axios';
import { API } from '../../configuration/configuration';
import { handleError } from './index';


export const searchPatients = (keyword, limit, callback) => dispatch => {

    axios.post(API + "patients/search?limit=" + 3, {keyword})
    .then(response => {
        callback(false, response.data);
    }).catch(error => {
        handleError(error, dispatch, callback);
    })

}


export const addPatient=(patient,callback)=>dispatch=>{
    
    axios.post(API+'patients',patient)
    .then(response=>{
        callback(false,response.data);
    }).catch(error=>{
        handleError(error,dispatch,callback);
    })
}
import axios from 'axios';
import { API } from '../../configuration/configuration';
import { handleError } from './index';


export const getAppointmentsForDate = (date, callback) => dispatch => {

    axios.get(API + 'appointments?date=' + date)
    .then(response => {
        callback(false, response.data);
    }).catch(error => {
        handleError(error, dispatch, callback);
    })


}

export const addAppointment = (appointment, callback) => dispatch => {

    axios.post(API+'appointments',appointment)
    .then(response=>{
        callback(false,response.data);
    }).catch(error=>{
        handleError(error,dispatch,callback);
    })
}

export const updateAppointment = (appointment, callback) => dispatch => {

    axios.post(API+'appointments',appointment)
    .then(response=>{
        callback(false,response.data);
    }).catch(error=>{
        handleError(error,dispatch,callback);
    })
}


export const deleteAppointment = (id, callback) => dispatch => {

    axios.delete(API+'appointments/'+id)
    .then(response=>{
        callback(false);
    }).catch(error=>{
        handleError(error,dispatch,callback);
    })
}
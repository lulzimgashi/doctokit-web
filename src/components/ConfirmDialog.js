import React, { Component } from 'react';

import Button from './Button'

export var confirmDialog = null;

class ConfirmDialog extends Component {
    state = { show: false, title: '' }
    callback = null;

    constructor() {
        super();
        confirmDialog = this;
    }

    showConfirm = (title, callback) => {
        this.setState({ show: true, title });
        this.callback = callback;
    }

    cancel = () => {
        this.setState({ show: false })
    }

    ok = () => {
        this.setState({ show: false })
        this.callback();
    }
    render() {
        if (!this.state.show) {
            return <div />
        }
        return (
            <div className='popup-w'>
                <div className='popup-form'>
                    <div className='popup-form__first'>
                        <span className='popup-form__name'>{this.state.title}</span>
                    </div>

                    <div className='popup-form__mid confirm-delete'>
                        <Button title='Cancel' click={this.cancel} className='dk-btn--small-height open-sans ' />
                        <Button title='Confirm' click={this.ok} className='dk-btn--small-height dk-btn--color-danger open-sans' />
                    </div>
                </div>

                <div className='popup-w__background'></div>
            </div>
        );
    }
}

export default ConfirmDialog;


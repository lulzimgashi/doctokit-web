import React, { Component } from 'react';

class Switch extends Component {
    state = { checked: false }


    componentWillMount() {
        this.setState({ checked: this.props.value });
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ checked: nextProps.value });
    }

    onChange = () => {
        if (this.props.onChange) {
            this.props.onChange(!this.state.checked);
        }
        this.setState({ checked: !this.state.checked });
    }

    render() {
        return (
            <div ref={ref => this.main = ref} className={'dk-input-w ' + this.props.className}>
                <label className="dk-switch">
                    <input type="checkbox" checked={this.state.checked} onChange={this.onChange} />
                    <span className="dk-switch-slider round"></span>
                </label>
                {this.props.label && <label className={'dk-input-w__label dk-input-w__label--on'}>{this.props.placeholder}</label>}
            </div>
        );
    }
}

export default Switch;
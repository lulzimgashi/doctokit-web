import React, { Component } from 'react';
import { connect } from 'react-redux';
import '../assets/css/siderbar.css';


class Sidebar extends Component {
    state = { id: 1 }
    navigate = (value) => {
        const current = this.props.history.location.pathname;
        if (value !== current) {
            this.props.history.push(value);
            this.setState({ id: this.state.id + 1 })
        }
    }
    render() {
        const current = this.props.history.location.pathname;
        const authorities = this.props.user.authorities;

        const admin = authorities.includes('ROLE_ADMIN');
        const appointments = authorities.includes('ROLE_APPOINTMENT');
        const patients = authorities.includes('ROLE_PATIENT');
        const costs = authorities.includes('ROLE_COST');
        const debts = authorities.includes('ROLE_DEBT');
        const reports = authorities.includes('ROLE_REPORT');
        const users = authorities.includes('ROLE_USER');


        return (
            <div className='sidebar'>
                <div className='sidebar__header'>DOCTOKIT</div>
                <div className='sidebar__items'>
                    <SidebarItem navigate={this.navigate} current={current} path='/' title='Dashboard' icon='dashboard' />
                    {(admin || appointments) && <SidebarItem navigate={this.navigate} current={current} path='/appointments' title='Appointments' icon='event_note' />}
                    {(admin || patients) && <SidebarItem navigate={this.navigate} current={current} path='/patients' title='Patients' icon='people' />}

                    {(admin || costs || debts || reports) && <div className='sidebar__divider'></div>}

                    {(admin || costs) && <SidebarItem navigate={this.navigate} current={current} path='/expenses' title='Costs' icon='money_off' />}
                    {(admin || debts) && <SidebarItem navigate={this.navigate} current={current} path='/debts' title='Debts' icon='monetization_on' />}
                    {(admin || reports) && <SidebarItem navigate={this.navigate} current={current} path='/reports' title='Reports' icon='assessment' />}

                    {(admin) && <div className='sidebar__divider'></div>}

                    {(admin || users) && <SidebarItem navigate={this.navigate} current={current} path='/users' title='Users' icon='account_circle' />}
                    {admin && <SidebarItem navigate={this.navigate} current={current} path='/services' title='Services' icon='airline_seat_flat_angled' />}
                    {admin && <SidebarItem navigate={this.navigate} current={current} path='/settings' title='Settings' icon='settings' />}
                </div>
            </div>
        );
    }
}


class SidebarItem extends Component {
    navigate = () => {
        this.props.navigate(this.props.path)
    }
    render() {
        return (
            <div onClick={this.navigate} className={'sidebar__item ' + (this.props.current === this.props.path ? 'sidebar__item--active' : '')}>
                <i className="material-icons sidebar__item-icon">{this.props.icon}</i>
                <span>{this.props.title}</span>
            </div>);
    }
}


const mapStateToProps = state => {
    return { loading: state.loading, user: state.user }
}

export default connect(mapStateToProps)(Sidebar);
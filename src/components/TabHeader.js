import React, { Component } from 'react';

class TabHeader extends Component {

    render() {
        return (
            <div className={'dk-tab-items-wrapper ' + (this.props.className)}>
                {this.props.children}
            </div>
        );
    }
}

export default TabHeader;
import React, { Component } from 'react';
//components
import InputRow from './../InputRow';
import Button from '../Button';
import { confirmDialog } from '../ConfirmDialog';
import ButtonPopup from '../ButtonPopup';
//actions
import { updateService, deleteService } from '../../redux/actions';


class ServiceTableRow extends Component {
    state = { editMode: false, loading: false }

    componentWillMount() {
        this.setState({ item: this.props.item })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ item: nextProps.item })
    }


    switchEditMode = (e) => {
        this.setState({ editMode: true });
    }

    save = () => {
        var item = this.state.item;

        if (!this.name.state.error && item.name !== this.name.input.value) {

            this.setState({ loading: true });

            var newItem = JSON.parse(JSON.stringify(item));
            newItem.name = this.name.input.value;

            this.props.dispatch(updateService(newItem, (error) => {
                if (error) {
                    this.setState({ editMode: false, loading: false })
                } else {
                    item.name = newItem.name;
                    this.setState({ editMode: false, loading: false })
                }
            }))
        } else {
            this.setState({ editMode: false })
        }
    }

    switchActive = () => {
        var item = this.state.item;
        this.setState({ loading: true });

        var newItem = JSON.parse(JSON.stringify(item));
        newItem.active = !item.active;

        this.props.dispatch(updateService(newItem, (error) => {
            if (error) {
                this.setState({ editMode: false, loading: false })
            } else {
                item.active = !item.active;
                this.setState({ editMode: false, loading: false })
            }
        }))
    }


    cancel = () => {
        this.setState({ editMode: false });
    }

    delete = () => {
        confirmDialog.showConfirm('Delete Service', () => {
            this.setState({ loading: true });

            this.props.dispatch(deleteService(this.state.item.id, (error) => {
                if (error) {
                    this.setState({ editMode: false, loading: false })
                } else {
                    this.props.delete(this.state.item.id);
                }
            }))
        });
    }

    render() {
        var value = this.state.item.name;
        var active = this.state.item.active;

        return (
            <div className='table-row-wrapper table-row-wrapper--border-bottom'>
                {!this.state.editMode && <span className='flex-1 table-margin'>{value}</span>}
                {this.state.editMode && <InputRow readOnly={this.state.loading} ref={ref => this.name = ref} className='min-height-28px flex-1 table-margin' value={value} />}

                <div className='min-width-110px table-margin'>
                    {this.state.editMode && !this.state.loading && <Button title='Save' click={this.save} className='dk-btn--small-height open-sans' />}
                    {!this.state.editMode && !this.state.loading && <Button title={active ? 'Active' : 'Inactive'} click={this.switchActive} className={'dk-btn--small-height open-sans em-1-i'} />}
                </div>

                {!this.state.loading &&
                    <div className='min-width-70px table-margin '>
                        {!this.state.editMode ?
                            <ButtonPopup icon='more_horiz' click={this.login} className='dk-btn--color-normal dk-btn--small-height dk-btn--border relative em-1-2'>
                                <div className={'drop-down ' + (this.props.renderPopupBottom ? 'drop-down--bottom' : '')}>
                                    <span className='drop-down__default-item' onClick={this.switchEditMode}>Edit</span>
                                    <span className='drop-down__default-item' onClick={this.delete}>Delete</span>
                                </div>
                            </ButtonPopup> :
                            <Button icon='cancel' click={this.cancel} className='dk-btn--small-height em-1' />}
                    </div>}

                {this.state.loading &&
                    <div className='min-width-70px table-margin position-relative'>
                        <Button loading className='dk-btn--small-height' />
                    </div>}
            </div>
        );
    }
}

export default ServiceTableRow;
import React, { Component } from 'react';
import Button from '../Button';
import Input from '../Input';
import Select from '../Select';
//actions
import { saveService } from '../../redux/actions';

class AddServicePopup extends Component {
    state = { loading: false }
    close = () => {
        this.props.parent.closePopup();
    }

    saveService = () => {
        var service = { name: this.name.input.value, active: this.select.state.item.value }
        this.setState({ popupLoading: true });

        this.props.dispatch(saveService(service, (error, item) => {
            if (error) {
                this.setState({ loading: false });
            } else {
                this.props.parent.addItem(item);
            }
        }))
    }

    render() {
        return (
            <div className='popup-w'>
                <div className='popup-form'>
                    <div className='popup-form__first'>
                        <span className='popup-form__name'>Add Service</span>
                    </div>

                    <div className='popup-form__mid add-service-popup'>
                        <Input ref={ref => this.name = ref} className='width-100percent' label placeholder='Name' />
                        <Select ref={ref => this.select = ref}
                            className='width-100percent mrt-30px'
                            label
                            placeholder='Status'
                            items={[{ name: 'Active', value: true }, { name: 'Inactive', value: false }]}
                            name='name' />
                    </div>

                    <div className='popup-form__bottom'>
                        <span />
                        <Button loading={this.state.loading} title='Add Service' click={this.saveService} className={'dk-btn--medium-height mrl-5px '} />
                    </div>
                    {!this.state.loading && <i onClick={this.close} className="material-icons popup-w__close-i">close</i>}
                </div>

                <div className='popup-w__background'></div>
            </div>
        );
    }
}

export default AddServicePopup;
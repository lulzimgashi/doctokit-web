import React, { Component } from 'react';

class BarChart extends Component {
    render() {
        return (
            <div className="dsh-chart dsh-row__item">
                <div className='dsh-chart__top'>
                    <span className='dsh-chart__total'>Cost & Debts</span>
                    <div>
                        <span className='dsh-chart__name'>Cost</span>
                        <span className='dsh-chart__name'>Debts</span>
                    </div>
                </div>
                {this.props.children}
            </div>
        );
    }
}

export default BarChart;
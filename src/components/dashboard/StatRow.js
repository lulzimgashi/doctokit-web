import React, { Component } from 'react';

class StatRow extends Component {
    render() {
        return (
            <div className='dsh-stat__row'>
                {this.props.children}
            </div>
        );
    }
}

export default StatRow;
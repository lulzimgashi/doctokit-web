import React, { Component } from 'react';

class Stats extends Component {
    render() {
        return (
            <div className="dsh-stats dsh-row__item dsh-row__item--right">
                {this.props.children}
            </div>
        );
    }
}

export default Stats;
import React, { Component } from 'react';
//components
import Loader from '../components/Loader';
import { Line, Bar, Pie } from 'react-chartjs-2';
import '../assets/css/dashboardnew.css';
import Row from '../components/dashboard/Row';
import Chart from '../components/dashboard/Chart';
import Stats from '../components/dashboard/Stats';
import StatRow from '../components/dashboard/StatRow';
import StatItem from '../components/dashboard/StatItem';




class Dashboard extends Component {
    state = { loading: false }
    componentDidMount() {

        var gradient = this.revenueChart.chartInstance.ctx.createLinearGradient(0, 0, 0, 400);
        gradient.addColorStop(0, 'rgba(69, 106, 235,.4)');
        gradient.addColorStop(0.6, 'rgba(255, 255, 255,.1)');
        gradient.addColorStop(1, 'rgba(255, 255, 255,.1)');

        this.revenueChart.chartInstance.data.datasets[0].backgroundColor = gradient;
        this.revenueChart.chartInstance.update();
    }

    refresh = () => {
        this.setState({ loading: true });
    }

    render() {
        return (
            <div className='main-container main-container--bg-grey'>
                <div className='main-container__header'>
                    <span className='main-container__title'>Dashboard</span>
                    {!this.state.loading ? <i onClick={this.refresh} className='material-icons main-container__reload-icon'>refresh</i> : <Loader color='#456AEB' className='mrl-10px' />}
                </div>

                <Row>
                    <Chart>
                        <Line ref={ref => this.revenueChart = ref} data={data} options={lineOptions} />
                    </Chart>

                    <Stats>
                        <StatRow>
                            <StatItem number={92} type='Clients' icon='supervisor_account' className='dsh-stats__item--blue' />
                            <StatItem number={32} type='Appointments' icon='event_note' className='dsh-stats__item--green' />
                        </StatRow>

                        <StatRow>
                            <StatItem number={430} type='Debts' icon='attach_money' className='dsh-stats__item--red' />
                            <StatItem number={130} type='Costs' icon='money_off' className='dsh-stats__item--yellow' />
                        </StatRow>
                    </Stats>
                </Row>

                <Row>
                    <BarChart>
                        <Bar ref={ref => this.barChart = ref} data={barData} options={lineOptions} />
                    </BarChart>
                </Row>
            </div>
        );
    }
}



export default Dashboard;





const data = {
    labels: ["1", "2", "3", "4", "5", "6", "7"],
    datasets: [
        {
            data: [65, 59, 80, 81, 56, 55, 60],
            borderColor: '#456AEB',
            borderWidth: 2,
            pointBorderColor: 'white',
            pointBackgroundColor: '#456AEB',
            pointBorderWidth: 1,
            pointRadius: 4,
            pointHoverRadius: 6
        }
    ]
};

const barData = {
    labels: ["1", "2", "3", "4", "5", "6", "7"],
    datasets: [
        {
            data: [65, 59, 80, 81, 56, 55, 60],
            backgroundColor: '#456AEB'
        },
        {
            data: [55, 69, 50, 61, 76, 35, 40],
            backgroundColor: '#58DEB5'
        }
    ]
};

const pieData = {
    labels: ["1", "2", "3", "4", "5"],
    datasets: [
        {
            data: [6, 16, 26, 36, 46],
            backgroundColor: ["rgb(252,194,60)", "rgb(196,79,67)", "rgb(82,194,160)", "rgb(72,110,232)", "rgb(233,38,137)"]
        }
    ]
};

const lineOptions = {
    responsive: true, maintainAspectRatio: false, legend: false, scales: {
        yAxes: [{
            gridLines: {
                color: '#F6F7F9',
                drawBorder: false,
                zeroLineColor: 'transparent'
            }, ticks: {
                maxTicksLimit: 5,
                min: 0,
                fontSize: 10
            }
        }],
        xAxes: [{
            gridLines: {
                color: '#F6F7F9',
                drawBorder: false,
                zeroLineColor: 'transparent'
            },
            ticks: {
                fontSize: 10
            },
            barPercentage: 0.6,
            categoryPercentage: 0.6,
        }],
    },
    tooltips: {
        backgroundColor: '#F6F7F9',
        borderColor: '#F6F7F9',
        displayColors: false,
        yAlign: 'bottom',
        bodyFontColor: 'grey',
        footerFontColor: 'grey',
        titleFontColor: 'grey',
        callbacks: {
            title: () => '',
            label: (tooltipItem, data) => {
                return '$ ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
            }
        }
    }
}

const pieOptions = {
    responsive: false, legend: false,
    cutoutPercentage: 80,
    legend: {
        display: false
    },
    animation: {
        animateRotate: true,
        animateScale: true
    },
    tooltips: {
        enabled: false
    }
}
import React, { Component } from 'react';

class StatItem extends Component {
    render() {
        return (
            <div className={'dsh-stats__item '+(this.props.className||'')}>
                <div>
                    <span className='dsh-stats__number'>{this.props.number}</span>
                    <span className='dsh-stats__type'>{this.props.type}</span>
                </div>
                <i className='material-icons dsh-stats__icon'>{this.props.icon}</i>
            </div>
        );
    }
}

export default StatItem;
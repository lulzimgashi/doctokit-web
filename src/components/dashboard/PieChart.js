import React, { Component } from 'react';
import { Pie } from 'react-chartjs-2';


class PieChart extends Component {
    render() {
        return (
            <div className="dsh-chart dsh-row__item dsh-chart--pie">
                <div className='dsh-chart__top'>
                    <span className='dsh-chart__total'>Total Revenue</span>
                    <span className='dsh-chart__name dsh-chart__name--no-dot'>Top 5</span>
                </div>

                <div className="dsh-chart__pie-c">
                    <Pie ref={ref => this.pieChart = ref} height={170} width={170} data={pieData} options={pieOptions} type='doughnut' />

                    <div className="dsh-chart__pie-absolute-span">
                        <span className="em-1-5">23</span>
                        <span className="em-0-9">Appointments</span>
                    </div>


                    <div className="dsh-pie-chart-rows-c">
                        <div className="dsh-chart__pie-row">
                            <span className="dsh-chart__pie-row-name c1">Theodore Yates</span>
                            <span className="dsh-chart__pie-row-number">6</span>
                        </div>

                        <div className="dsh-chart__pie-row">
                            <span className="dsh-chart__pie-row-name c2">Theodore Yates</span>
                            <span className="dsh-chart__pie-row-number">6</span>
                        </div>

                        <div className="dsh-chart__pie-row">
                            <span className="dsh-chart__pie-row-name c3">Theodore Yates</span>
                            <span className="dsh-chart__pie-row-number">6</span>
                        </div>

                        <div className="dsh-chart__pie-row">
                            <span className="dsh-chart__pie-row-name c4">Theodore Yates</span>
                            <span className="dsh-chart__pie-row-number">6</span>
                        </div>

                        <div className="dsh-chart__pie-row">
                            <span className="dsh-chart__pie-row-name c5">Theodore Yates</span>
                            <span className="dsh-chart__pie-row-number">6</span>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}

export default PieChart;


const pieData = {
    labels: ["1", "2", "3", "4", "5"],
    datasets: [
        {
            data: [6, 16, 26, 36, 46],
            backgroundColor: ["rgb(252,194,60)", "rgb(196,79,67)", "rgb(82,194,160)", "rgb(72,110,232)", "rgb(233,38,137)"]
        }
    ]
};




const pieOptions = {
    responsive: false, legend: false,
    cutoutPercentage: 80,
    legend: {
        display: false
    },
    animation: {
        animateRotate: true,
        animateScale: true
    },
    tooltips: {
        enabled: false
    }
}
import React, { Component } from 'react';

class Chart extends Component {
    render() {
        return (
            <div className="dsh-chart dsh-row__item">
                <div className='dsh-chart__top'>
                    <span className='dsh-chart__total'>Total Revenue</span>
                    <span className='dsh-chart__name'>Profit</span>
                </div>
                {this.props.children}
            </div>
        );
    }
}

export default Chart;
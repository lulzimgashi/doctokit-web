import React, { Component } from 'react';
import { withRouter } from 'react-router'
//actions
import { deleteAppointment } from '../../redux/actions';

import Button from '../Button';


class ViewAppointmentPopup extends Component {
    state = { deleteMode: false, deleteLoading: false }

    close = () => {
        this.props.close();
    }
    goToPatient = () => {
        this.props.history.push('patients/' + this.props.appointment.patientId);
    }

    switchDeleteMode = () => {
        this.setState({ deleteMode: !this.state.deleteMode })
    }

    delete = () => {
        const event = this.props.appointment;

        this.setState({ deleteLoading: true, deleteMode: false });

        this.props.parent.props.dispatch(deleteAppointment(event.id, (error) => {
            if (!error) {
                this.props.parent.eventDeleted();
            }
        }))
    }

    render() {

        const event = this.props.appointment;

        return (
            <div className='popup-w'>
                <div className='popup-form'>
                    <div className='popup-form__first'>
                        <span className='popup-form__name'>{event.start.format('dddd MMM 10')}</span>
                        <span className='popup-form__second-name'>{event.start.format('HH:mm')} - {event.end.format('HH:mm')}</span>
                    </div>



                    {this.state.deleteMode ?
                        (<div className='event-confirm-delete'>
                            <h3>Confirm Delete</h3>
                            <div className='event-confirm-delete__buttons'>
                                <Button title='No' click={this.switchDeleteMode} className='dk-btn--color-grey dk-btn--medium-height' />
                                <Button title='Yes' click={this.delete} className='dk-btn--color-danger mrl-5px dk-btn--medium-height' />
                            </div>
                        </div>) :
                        (<div className='popup-form__mid view-event-popup'>
                            <div className='view-event-popup__col'>
                                <div className='view-event-popup__name-w'>
                                    <i className='material-icons view-event-popup__name-i'>contact_mail</i>Name
                                </div>
                                <span className='view-event-popup__value'>{event.firstName + ' ' + event.lastName}</span>
                            </div>

                            <div className='view-event-popup__col view-event-popup__col--mid-item'>
                                <div className='view-event-popup__name-w'>
                                    <i className='material-icons view-event-popup__name-i'>wc</i>Gender
                                </div>
                                <span className='view-event-popup__value'>{event.gender}</span>
                            </div>

                            <div className='view-event-popup__col'>
                                <div className='view-event-popup__name-w'>
                                    <i className='material-icons view-event-popup__name-i'>local_phone</i>Phone
                                </div>
                                <span className='view-event-popup__value'>{event.phone}</span>
                            </div>
                        </div>)}


                    <div className='popup-form__bottom'>
                        {this.state.deleteMode ? <span /> : <Button loading={this.state.deleteLoading} title='Delete' click={this.switchDeleteMode} className='dk-btn--color-danger dk-btn--medium-height' />}
                        <Button loading={this.props.loading} title='View Patient' click={this.goToPatient} className='dk-btn--medium-height mrl-15px' />
                    </div>

                    {!this.state.loading && <i onClick={this.close} className="material-icons popup-w__close-i">close</i>}
                </div>

                <div className='popup-w__background'></div>
            </div>
        );
    }
}

export default withRouter(ViewAppointmentPopup, { withRef: true });
import React, { Component } from 'react';
import { connect } from 'react-redux'
import Button from '../Button';
import moment from 'moment';
//actions
import { searchPatients, addAppointment } from '../../redux/actions';

class SearchUserPopup extends Component {
    state = { items: [], selected: null, loading: false }

    close = () => {
        this.props.close();
    }
    ok = () => {
        this.props.ok();
    }

    onChange = (e) => {
        const keyword = this.searchInput.value;

        if (keyword.length > 2) {
            clearTimeout(this.searchTimeOut)
            this.props.dispatch(searchPatients(keyword, 5, (error, data) => {
                if (!error) {
                    this.setState({ items: data });
                }
            }))
        } else {
            this.setState({ items: [] })
        }
    }

    clear = () => {
        this.setState({ selected: null })
        this.searchInput.value = '';
    }

    itemClicked = (item) => {
        this.setState({ selected: item, items: [] });
        this.searchInput.value = item.firstName + ' ' + item.lastName;
    }

    renderRows = () => {
        return this.state.items.map((item, i) => {
            return (
                <div key={item.id} className='drop-down__default-item' onClick={() => this.itemClicked(item)}>
                    <span className='flex-1'>{item.firstName + ' ' + item.lastName}</span>
                    <span className='drop-down__phone-number'>{item.phone}</span>
                    <i className='material-icons em-1-2'>chevron_right</i>
                </div>);
        })

    }

    switchToAddPopup = () => {
        this.props.parent.switchEventPopup(1);
    }

    makeAppointmentClicked = () => {
        const event = this.props.event;

        var appointment = {
            createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
            date: event.start.format('YYYY-MM-DD'),
            start: event.start.format('HH:mm'),
            end: event.end.format('HH:mm'),
            doctorId: this.props.doctor.id,
            patientId: this.state.selected.id,
            firstName: this.state.selected.firstName,
            lastName: this.state.selected.lastName,
            phone: this.state.selected.phone,
            gender: this.state.selected.gender
        }

        this.setState({ loading: true });

        this.props.dispatch(addAppointment(appointment, (error, data) => {
            if (error) {
                this.setState({ loading: true });
            }
            else {
                appointment.id = data.id;
                this.props.parent.eventAddedSuccesfully(appointment);
            }
        }))
    }

    render() {

        const { selected } = this.state;
        const { event } = this.props;

        return (
            <div className='popup-w'>
                <div className='popup-form'>
                    <div className='popup-form__first'>
                        <span className='popup-form__name'>{event.start.format('dddd MMM 10')}</span>
                        <span className='popup-form__second-name'>{event.start.format('HH:mm')} - {event.end.format('HH:mm')}</span>
                    </div>


                    <div className='popup-form__mid search-patient-form'>
                        <div className='dk-input-n-icon'>
                            <i className='material-icons dk-input-n-icon__icon'>search</i>
                            <input className={'dk-input-n-icon__input ' + (selected ? 'dk-input-n-icon__input--has-data' : '')} readOnly={selected} ref={ref => this.searchInput = ref} placeholder='Search Patients' onChange={this.onChange} />

                            {this.state.items.length > 0 &&
                                <div className='drop-down drop-down--small-height'>
                                    {this.renderRows()}
                                </div>}

                            {selected && <i onClick={this.clear} className='material-icons dk-input-n-icon__clear-icon'>clear</i>}
                        </div>
                    </div>


                    <div className='popup-form__bottom'>
                        {this.state.loading ? <span /> : <Button title='Add' click={this.switchToAddPopup} className='dk-btn--color-normal dk-btn--medium-height dk-btn--border' />}
                        <Button loading={this.state.loading} title='Make Appointment' click={this.makeAppointmentClicked} className={'dk-btn--medium-height mrl-5px ' + (selected ? 'dk-btn--active' : 'dk-btn--color-grey')} />
                    </div>

                    {!this.state.loading && <i onClick={this.close} className="material-icons popup-w__close-i">close</i>}
                </div>

                <div className='popup-w__background'></div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {}
}

export default connect(mapStateToProps)(SearchUserPopup);

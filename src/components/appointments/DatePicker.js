import React, { Component } from 'react';
import 'react-date-range/dist/styles.css'; // main style file
import 'react-date-range/dist/theme/default.css'; // theme css file
import { Calendar } from 'react-date-range';
//
import moment from 'moment';


class DatePicker extends Component {
    state = { date: moment(), datePickerOn: false }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }


    handleClickOutside = (event) => {
        if (this.state.datePickerOn && (this.wrapperRef && !this.wrapperRef.contains(event.target))) {
            if (this.state.datePickerOn) {
                this.setState({ datePickerOn: false })
            }
        }
    }

    switchDatePicker = () => {
        this.setState({ datePickerOn: !this.state.datePickerOn })
    }

    dateChanged = (date) => {
        var selectedDate = moment(date);


        if (selectedDate.format('DD-MM-YY') === this.state.date.format('DD-MM-YY')) {
            this.setState({ datePickerOn: false });
        } else {
            if (this.props.onChange) {
                this.props.onChange(selectedDate);
            }
            this.setState({ date: selectedDate, datePickerOn: false });
        }
    }

    render() {

        const { datePickerOn, date } = this.state;

        return (
            <div ref={divi => this.wrapperRef = divi} className={'appointments-cal-w appointments-header__item ' + (this.props.className || '')}>
                <span className='appointments-cal-w__label'>{this.props.label}</span>

                <div onClick={this.switchDatePicker} className='appointments-cal-w__last'>
                    <span className='appointments-cal-w__date'>{date.format('DD MMM YYYY')}</span>
                    <i className="material-icons drop-down-arrow-down">arrow_drop_down</i>
                </div>

                {datePickerOn && <div className={'appointments-cal-w__cal ' + (this.props.right ? 'appointments-cal-w__cal--right' : '')}>
                    <Calendar
                        options={{
                            color: '#456AEB'
                        }}
                        date={this.state.date}
                        onChange={this.dateChanged} />
                </div>}
            </div>
        );
    }
}

export default DatePicker;
import React, { Component } from 'react';
import moment from 'moment';
//actions
import { updateAppointment } from '../../redux/actions'
// components
import $ from 'jquery'
import 'fullcalendar'
import 'fullcalendar/dist/fullcalendar.css';
import '../../assets/css/fullcalendar.css';


class Calendar extends React.Component {

    componentDidMount() {
        this.firstCalDate = moment();
        this.getFirstAndEndTime();
        var defaultSettings = this.getDefaultSettings();


        $(this.calendar).fullCalendar({
            slotDuration: '00:15:00',
            dayCount: 5,
            defaultView: 'agenda',
            editable: true,
            selectable: true,
            selectHelper: true,
            allDaySlot: false,
            eventOverlap: false,
            slotEventOverlap: false,
            selectOverlap: false,
            slotLabelFormat: 'HH:mm',
            contentHeight: 'auto',
            minTime: defaultSettings.startTime.format('HH:mm:ss'),
            maxTime: defaultSettings.endTime.format('HH:mm:ss'),
            header: {
                right: 'agenda,agendaDay',
                left: ''
            },
            eventRender: this.eventRender,
            eventClick: this.eventClick,
            select: this.eventSelect,
            eventResize: this.eventResize,
            eventDrop: this.eventResize,
            events: defaultSettings.events
        });

    }

    componentWillUnmount() {
        $(this.calendar).fullCalendar('destroy');
    }

    componentWillReceiveProps(nextProps) {
        var events = [];

        if (!nextProps.doctor) {
            return;
        }

        for (var e in nextProps.doctor.appointments) {
            var appointment = nextProps.doctor.appointments[e];

            var event = {
                id: appointment.id,
                date: appointment.date,
                start: appointment.date + ' ' + appointment.start,
                end: appointment.date + ' ' + appointment.end,
                patientId: appointment.patientId,
                doctorId: appointment.doctorId,
                firstName: appointment.firstName,
                lastName: appointment.lastName,
                phone: appointment.phone,
                gender: appointment.gender,
                createdAt: appointment.createdAt
            }

            events.push(event);
        }

        var defaultSettings = this.getDefaultSettings();



        events.push(...defaultSettings.events);

        $(this.calendar).fullCalendar('removeEvents')
        $(this.calendar).fullCalendar('addEventSource', events)
        // $(this.calendar).fullCalendar('rerenderEvents');

    }

    getFirstAndEndTime = () => {
        const clinic = this.props.parent.props.user.clinic;

        var days = [];

        var startTime;
        var endTime;

        for (var e in clinic.days) {
            var day = clinic.days[e];


            var newDay = { day: day.day, start: day.start, end: day.end, open: day.open }
            if (newDay.day === 7) {
                newDay.day = 0;
            }

            if (!startTime) {
                startTime = moment('2016-01-01 ' + day.start);
                endTime = moment('2016-01-01 ' + day.end);
                newDay.start = startTime;
                newDay.end = endTime;
            } else {
                var start = moment('2016-01-01 ' + day.start);
                var end = moment('2016-01-01 ' + day.end);

                newDay.start = start;
                newDay.end = end;

                if (start.isBefore(startTime)) {
                    startTime = start;
                }
                if (end.isAfter(endTime)) {
                    endTime = end;
                }
            }
            days.push(newDay);
        }


        this.startTime = startTime;
        this.endTime = endTime;
        this.days = days;
    }

    getDefaultSettings = () => {
        var dateRange = this.getDateRanges(this.firstCalDate, 5);

        var events = [];

        for (var e in dateRange) {
            var date = dateRange[e];
            for (var i in this.days) {
                var day = this.days[i];


                if ((date.get('day').toString() === day.day.toString()) && !day.open) {

                    var event = {
                        start: date.format('YYYY-MM-DD') + ' ' + this.startTime.format('HH:mm:ss'),
                        end: date.format('YYYY-MM-DD') + ' ' + this.endTime.format('HH:mm:ss'),
                        rendering: 'background'
                    }

                    events.push(event);
                }
            }
        }

        return { startTime: this.startTime, endTime: this.endTime, events }
    }

    getDateRanges = (start, days) => {
        var range = [start];

        for (let index = 1; index < days; index++) {
            range.push(moment(start).add(index, 'd'));
        }

        return range;
    }

    goToDate = (date) => {
        this.firstCalDate = date;
        $(this.calendar).fullCalendar('gotoDate', date);
    }

    eventRender = (event, element, view) => {

        var content = element.find('.fc-content')
        content.empty();
        var html = '';


        html += "<span class='name'>" + (event.firstName ? event.firstName + ' ' + event.lastName : ' ') + "</span>"

        if (event.end) {
            html += "<span class='time'>" + event.start.format('HH:mm') + '-' + event.end.format('HH:mm') + "</span>"
        } else {
            html += "<span class='time'>" + event.start.format('HH:mm') + "</span>"
        }
        content.append(html);

    }

    eventClick = (event) => {
        this.props.parent.eventClicked(event);
    }

    eventSelect = (start, end) => {
        const event = { start, end }
        this.props.parent.eventSelected(event);
    }

    eventResize = (event, delta, revertFunc) => {
        var newEvent = {
            id: event.id,
            date: event.start.format('YYYY-MM-DD'),
            start: event.start.format('HH:mm:ss'),
            end: event.end.format('HH:mm:ss'),
            patientId: event.patientId,
            doctorId: event.doctorId,
            createdAt: event.createdAt
        }



        this.props.dispatch(updateAppointment(newEvent, (error) => {
            if (error) {
                revertFunc();
            } else {
                for (var e in this.props.doctor.appointments) {
                    if (this.props.doctor.appointments[e].id === event.id) {
                        this.props.doctor.appointments[e].date = newEvent.date;
                        this.props.doctor.appointments[e].start = newEvent.start;
                        this.props.doctor.appointments[e].end = newEvent.end;
                    }
                }
            }
        }))
    }

    render() {

        return <div className='mrt-40px' ref={ref => this.calendar = ref} />
    }
}

export default Calendar;





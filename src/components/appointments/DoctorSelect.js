import React, { Component } from 'react';

class Select extends Component {
    state = { inputValue: '', error: false, on: false, index: 0, item: {} }

    componentWillMount() {
        if (this.props.items.length > 0) {
            const selected = this.props.selected;
            this.setState({ items: this.props.items, item: this.props.items[selected], index: selected })
        } else {
            this.setState({ items: [], item: { firstName: '', lastName: '', clinicRoles: [], appointments: [] } })
        }
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    componentWillReceiveProps(nextProps) {

        if (this.props.items !== nextProps.items) {
            if (nextProps.items.length > 0) {
                const selected = nextProps.selected ? nextProps.selected : 0;

                this.setState({ items: nextProps.items, item: nextProps.items[selected], index: selected })
            } else {
                this.setState({ items: [], item: { firstName: '', lastName: '', clinicRoles: [], appointments: [] } })
            }
        }
    }


    handleClickOutside = (event) => {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            if (this.state.on) {
                this.setState({ on: false })
            }
        }
    }

    onFocus = () => {
        this.setState({ on: true })
    }

    showDropDown = () => {
        if (this.state.items.length > 1) {
            this.setState({ on: !this.state.on });
        }
    }

    chosen = (item, i) => {
        if (this.props.onChange) {
            this.props.onChange(item, i);
        }
        this.setState({ on: false, index: i, item: item });
    }

    renderRows = () => {
        return this.state.items.map((doctor, i) => {
            return (
                <div key={i} className={'drop-down__default-item'} onClick={() => this.chosen(doctor, i)}>
                    <span>{doctor.firstName + ' ' + doctor.lastName}</span>
                    {doctor.clinicRoles.length > 0 ? <span className='color-light-grey open-sans'>{doctor.clinicRoles[0].name}</span> : ''}
                </div>
            )
        })
    }


    render() {
        const item = this.state.item;

        console.log(this.state.on);

        return (
            <div ref={divi => this.wrapperRef = divi} className={'appointments-doctors-w appointments-header__item mrr-30px ' + this.props.className} style={{ zIndex: 5 }}>

                <span className='initial-circle'>{item.appointments.length}</span>

                <div className='appointments-doctors-w__name-title'>
                    <span className='color-primary font-medium' >{item.firstName + ' ' + item.lastName}</span>
                    {item.clinicRoles.length > 0 ? <span className='color-grey  open-sans em-0-8 weight-100'>{item.clinicRoles[0].name}</span> : ''}
                </div>

                <div onClick={this.showDropDown} className='appointments-doctors-w__last mrr-10px'>
                    <span className='color-primary font-book em-0-8'>Change</span>
                    <i className="material-icons drop-down-arrow-down">arrow_drop_down</i>
                </div>

                {this.state.on &&
                    <div className='drop-down'>
                        {this.renderRows()}
                    </div>}
            </div>
        );
    }
}


Select.defaultProps = {
    selected: 0
}

export default Select;
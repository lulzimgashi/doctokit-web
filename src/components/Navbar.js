import React, { Component } from 'react';

class Navbar extends Component {
    render() {
        return (
            <div className='dk-navbar'>
                <i onClick={this.props.switch} className='material-icons sideber-switcher'>dehaze</i>
            </div>
        );
    }
}

export default Navbar;
import React, { Component } from 'react';

class SearchInput extends Component {
    render() {
        return (
            <div className={'dk-input-n-icon ' + this.props.className}>
                {this.props.icon && <i className='material-icons dk-input-n-icon__icon' >search</i>}
                <input className='dk-input-n-icon__input' onChange={e => this.props.onChange(e.target.value)} ref={ref => this.input = ref} placeholder={this.props.placeholder} />
            </div>
        );
    }
}

export default SearchInput;
import React, { Component } from 'react';
import { validateEmail } from '../index';

class Select extends Component {
    state = { inputValue: '', error: false, on: false, index: 0, item: {} }

    componentWillMount() {
        const selected = this.props.selected;
        this.setState({ items: this.props.items, item: this.props.items[selected], inputValue: this.props.items[selected][this.props.name], index: selected })
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleClickOutside = (event) => {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            this.setState({ on: false })
        }
    }

    onFocus = () => {
        this.setState({ on: true })
    }

    showDropDown = () => {
        this.setState({ on: true });
    }

    chosen = (item, i) => {
        if (this.props.onChange) {
            this.props.onChange(item, i);
        }
        this.setState({ on: false, index: i, item: item, inputValue: item[this.props.name] });
    }

    renderRows = () => {
        return this.state.items.map((item, i) => {
            return (
                <div key={i} className='drop-down__default-item' onClick={() => this.chosen(item, i)}>
                    <span>{item[this.props.name]}</span>
                </div>
            )
        })
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.state.on !== nextState.on) {
            return true
        }
        return false;
    }


    render() {
        const error = this.props.error || this.state.error ? ' error' : '';
        return (
            <div ref={divi => this.wrapperRef = divi} className={'dk-input-w ' + this.props.className}>
                <input readOnly type={this.props.type} ref={input => this.input = input} onChange={this.onChange} className={'dk-input ' + (error)} onFocus={this.onFocus} onBlur={this.onBlur} value={this.state.item[this.props.name]} />
                <div className={'dk-input-w__border' + (error)}></div>
                {this.props.label && <label className={'dk-input-w__label' + (error) + (this.state.on || this.state.inputValue.length > 0 ? ' dk-input-w__label--on' : '')}>{this.props.placeholder}</label>}
                <i onClick={this.showDropDown} className="material-icons dk-input-w__icon">arrow_drop_down</i>

                {this.state.on &&
                    <div className='drop-down'>
                        {this.renderRows()}
                    </div>}
            </div>
        );
    }
}


Select.defaultProps = {
    selected: 0
}

export default Select;
import React, { Component } from 'react';
import GoogleMapsLoader from 'google-maps'

class LocationAutoSuggest extends Component {
    state = { inputValue: '', result: [], on: false, chosenPlace: null }


    componentWillMount() {
        const location = this.props.location;

        var chosenPlace = {
            geometry: {
                location: {
                    lng: location.lng,
                    ltd: location.ltd
                }
            },
            formatted_address: location.locationName
        }

        this.setState({ chosenPlace, inputValue: location.locationName });
    }


    componentDidMount() {
        // GoogleMapsLoader.KEY = 'AIzaSyCoq4_-BeKtYRIs-3FjJL721G1eP5DaU0g';    
        GoogleMapsLoader.LIBRARIES = ['places']
        GoogleMapsLoader.load((google) => {
            var aService = new google.maps.places.AutocompleteService();
            var placeService = new google.maps.places.PlacesService(document.createElement('div'));
            this.aService = aService;
            this.placeService = placeService;

        })
        document.addEventListener('mousedown', this.handleClickOutside);;
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleClickOutside = (event) => {
        if (this.main && !this.main.contains(event.target)) {
            if (this.state.chosenPlace) {
                this.setState({ on: false, inputValue: this.state.chosenPlace.formatted_address })
            } else {
                this.setState({ on: false, inputValue: '' })
            }
        }
    }

    searchLocation = (value) => {
        this.aService.getPlacePredictions({ input: value }, (results) => {
            var newResults = [];
            for (var e in results) {
                var res = {};
                res.description = results[e].description
                res.place_id = results[e].place_id;
                newResults.push(res);
            }

            this.setState({ result: newResults });
        });
    }

    onChange = (e) => {
        const value = e.target.value;

        if (value.length > 0) {
            this.searchLocation(value);
            this.setState({ inputValue: value });
        } else {
            this.setState({ inputValue: value, result: [] });
        }
    }

    onFocus = () => {
        this.setState({ on: true });
    }


    locationChosen = (item) => {
        this.setState({ inputValue: item.description, result: [], on: false });

        this.placeService.getDetails({ placeId: item.place_id }, (place) => {
            place.geometry.location.lng = place.geometry.location.lng();
            place.geometry.location.ltd = place.geometry.location.lat();

            if (this.props.onChange) {
                this.props.onChange(place);
            }
            this.setState({ chosenPlace: place });
        })

    }

    renderRows = () => {
        return this.state.result.map((item, i) => {
            return (
                <div key={item.place_id} className='drop-down__default-item ' onClick={() => this.locationChosen(item, i)}>
                    <span >{item.description}</span>
                </div>
            )
        })
    }

    render() {

        return (
            <div ref={ref => this.main = ref} className={'dk-input-w ' + (this.props.className || '')}>
                <input ref={input => this.input = input} onChange={this.onChange} className={'dk-input'} value={this.state.inputValue} onFocus={this.onFocus} />
                <div className={'dk-input-w__border'}></div>
                {this.props.label && <label className={'dk-input-w__label' + (this.state.on || this.state.inputValue.length > 0 ? ' dk-input-w__label--on' : '')}>{this.props.placeholder}</label>}

                {(this.state.on && this.state.result.length > 0) &&
                    <div className='drop-down'>
                        {this.renderRows()}
                    </div>}
            </div>
        );
    }
}

export default LocationAutoSuggest;
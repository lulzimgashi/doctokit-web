import React, { Component } from 'react';

class TabHeaderItem extends Component {
    render() {
        return (
            <div onClick={()=>this.props.onClick(this.props.index)} className={'dk-tab-item '+(this.props.className)+(this.props.active? ' active':'')}>
                <i className='material-icons'>{this.props.icon}</i>
                <span>{this.props.name}</span>
            </div>
        );
    }
}

export default TabHeaderItem;
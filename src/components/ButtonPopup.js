import React, { Component } from 'react';
import Loader from './Loader';

class ButtonPopup extends Component {
    state = { popup: false }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    click = (e) => {
        if (this.props.icon) {
            this.setState({ popup: !this.state.popup })
        }
        else {
            if (!this.props.loading) {
                this.props.click();
            }
        }
    }

    handleClickOutside = (event) => {
        if (this.state.popup && (this.wrapperRef && !this.wrapperRef.contains(event.target))) {
            if (this.state.popup) {
                this.setState({ popup: false })
            }
        }
    }

    render() {
        if (this.props.icon) {
            return (
                <div ref={ref => this.wrapperRef = ref} onClick={this.click} className={'dk-btn ' + (this.props.className || '')}>
                    <i className='material-icons em-1'>{this.props.icon}</i>
                    {this.state.popup && this.props.children}
                </div>);
        }
        return (
            <div onClick={this.click} className={this.props.className}>
                {!this.props.loading && this.props.title}
                {this.props.loading && <i className='material-icons em-1'>{this.props.icon}</i>}
            </div>
        );
    }
}

export default ButtonPopup;
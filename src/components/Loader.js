import React, { Component } from 'react';

class Loader extends Component {
    render() {
        return (
            <div className={"loader "+(this.props.className)}>
                <svg className="circular" viewBox="25 25 50 50">
                    <circle style={{stroke:this.props.color}} className="path" cx="50" cy="50" r="20" fill="none" strokeWidth="2" strokeMiterlimit="10" />
                </svg>
            </div>
        );
    }
}

export default Loader;
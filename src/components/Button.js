import React, { Component } from 'react';
import Loader from '../components/Loader';

class Button extends Component {
    state = { popup: false }
    click = (e) => {
        if (!this.props.loading) {
            this.props.click();
        }
    }

    onMouseLeave = () => {
        if (this.state.popup) {
            this.setState({ popup: false })
        }
    }
    render() {
        if (this.props.popup) {
            return (
                <div onClick={this.click} onMouseLeave={this.onMouseLeave} className={this.props.className}>
                    <i className='material-icons em-1'>{this.props.icon}</i>
                    {this.state.popup && this.props.children}
                </div>);
        }

        return (
            <div onClick={this.click} className={'dk-btn ' + (this.props.className || '')}>
                {this.props.loading ? <Loader color='white' /> :
                    (this.props.title || <i className='icon em-1-2 icon--color-inherit'>{this.props.icon}</i>)}
            </div>
        );
    }
}

export default Button;
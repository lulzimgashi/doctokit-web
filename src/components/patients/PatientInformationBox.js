import React, { Component } from 'react';

//Components
import Button from '../Button';
import Select from '../Select';
import loading from '../../redux/reducers/loading';

class PatientInformationBox extends Component {

    state = { showReportsPopup: false, editMode: false, loading: false }

    switchReportsPopup = () => {
        if (!this.state.showReportsPopup) {
            this.setState({ showReportsPopup: true });
        }
    }


    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleClickOutside = (event) => {
        if (this.state.showReportsPopup && (this.wrapperRef && !this.wrapperRef.contains(event.target))) {
            this.setState({ showReportsPopup: false })
        }
    }

    switchEditMode = () => {
        this.setState({ editMode: !this.state.editMode });
    }

    save = () => {
        this.setState({ loading: true, editMode: false });
        setTimeout(() => {
            this.setState({ loading: false })
        }, 1000);
    }

    render() {

        const { editMode, showReportsPopup, loading } = this.state;


        return (
            <div className='p-boxes__item p-info-box'>

                <div className='p-info-box__first'>

                    <span className='initial-circle initial-circle--large mrl-0px'>BH</span>
                    <div className='p-info-box__column-dir'>
                        <input className={'p-info-box__input p-info-box__input--name ' + (editMode ? 'p-info-box__input--active' : '')} readOnly={!editMode} defaultValue='Berat Hoxha' />
                        <input className={'p-info-box__input p-info-box__input--email ' + (editMode ? 'p-info-box__input--active' : '')} readOnly={!editMode} defaultValue='berathoxha@gmail.com' />
                    </div>


                    <div className='p-info-box__reports-dd-w' onClick={this.switchReportsPopup}>
                        <span>Reports</span>
                        <i className='material-icons mrl-10px em-1-5'>picture_as_pdf</i>
                        {showReportsPopup && <div ref={ref => this.wrapperRef = ref} className={'drop-down drop-down--width-auto '}>
                            <div className='drop-down__default-item' >History <i className='material-icons em-1-2 mrl-10px'>history</i> </div>
                            <div className='drop-down__default-item' >Payments <i className='material-icons em-1-2 mrl-10px'>attach_money</i> </div>
                            <div className='drop-down__default-item' >Debts <i className='material-icons em-1-2 mrl-10px'>money_off</i> </div>
                        </div>}
                    </div>
                </div>

                <div className='p-info-box__second'>
                    <div className='p-info-box__row'>

                        <div className='p-info-box__row-item'>
                            <div className='p-info-box__row__item-name-w' >
                                <i className='material-icons p-info-box__row-item-i'>wc</i>Gender
                            </div>
                            <input readOnly={!editMode} defaultValue='Mashkull' className={'p-info-box__input p-info-box__input--value ' + (editMode ? 'p-info-box__input--active' : '')} />
                        </div>

                        <div className='p-info-box__row-item'>
                            <div className='p-info-box__row__item-name-w' >
                                <i className='material-icons p-info-box__row-item-i'>event_available</i>Reservation
                            </div>
                            <input readOnly={!editMode} defaultValue='23 Done' className={'p-info-box__input p-info-box__input--value ' + (editMode ? 'p-info-box__input--active' : '')} />
                        </div>

                        <div className='p-info-box__row-item'>
                            <div className='p-info-box__row__item-name-w' >
                                <i className='material-icons p-info-box__row-item-i'>event_busy</i>Reservation
                            </div>
                            <input readOnly={!editMode} defaultValue='10 Canceled' className={'p-info-box__input p-info-box__input--value ' + (editMode ? 'p-info-box__input--active' : '')} />
                        </div>

                    </div>

                    <div className='p-info-box__row'>

                        <div className='p-info-box__row-item'>
                            <div className='p-info-box__row__item-name-w' >
                                <i className='material-icons p-info-box__row-item-i'>phone</i>Phone
                            </div>
                            <input readOnly={!editMode} defaultValue='+383 45 522 33' className={'p-info-box__input p-info-box__input--value ' + (editMode ? 'p-info-box__input--active' : '')} />
                        </div>

                        <div className='p-info-box__row-item'>
                            <div className='p-info-box__row__item-name-w' >
                                <i className='material-icons p-info-box__row-item-i'>cake</i>Birth Date
                            </div>
                            <input readOnly={!editMode} defaultValue='12/23/1190' className={'p-info-box__input p-info-box__input--value ' + (editMode ? 'p-info-box__input--active' : '')} />
                        </div>

                        <div className='p-info-box__row-item'>
                            <div className='p-info-box__row__item-name-w' >
                                <i className='material-icons p-info-box__row-item-i'>place</i>Address
                            </div>
                            <input readOnly={!editMode} defaultValue='Drenas, Arllat' className={'p-info-box__input p-info-box__input--value ' + (editMode ? 'p-info-box__input--active' : '')} />
                        </div>

                    </div>
                </div>

                <div className='p-info-box__last'>
                    {(editMode && !loading) && <Button click={this.switchEditMode} title='Cancel' className='dk-btn--color-normal dk-btn--small-height dk-btn--border open-sans mrr-10px' />}
                    <Button loading={this.state.loading} click={editMode ? this.save : this.switchEditMode} title={editMode ? 'Save' : 'Edit'} className='dk-btn--small-height open-sans' />
                </div>

            </div>
        );
    }
}

export default PatientInformationBox;
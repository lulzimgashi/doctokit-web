import React, { Component } from 'react';

class TabHeader extends Component {
    state = { tab: 0 }

    switchTab = (tab) => {
        if (this.state.tab !== tab) {
            this.setState({ tab });
            this.props.changeTab(tab);
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.state !== nextState) {
            return true;
        }

        return false;
    }


    render() {

        const { tab } = this.state;

        return (
            <div className='p-tab-header'>
                <span onClick={() => this.switchTab(0)} className={'p-tab-header__item ' + (tab === 0 ? 'p-tab-header__item--active' : '')} >History</span>
                <span onClick={() => this.switchTab(1)} className={'p-tab-header__item ' + (tab === 1 ? 'p-tab-header__item--active' : '')} >Appointments</span>
                <span onClick={() => this.switchTab(2)} className={'p-tab-header__item ' + (tab === 2 ? 'p-tab-header__item--active' : '')} >Attachments</span>
                <span onClick={() => this.switchTab(3)} className={'p-tab-header__item ' + (tab === 3 ? 'p-tab-header__item--active' : '')} >Payments</span>
            </div>
        );
    }
}

export default TabHeader;
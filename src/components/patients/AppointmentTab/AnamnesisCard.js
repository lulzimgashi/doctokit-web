import React, { Component } from 'react';
import ButtonPopup from '../../ButtonPopup';

class AnamnesisCard extends Component {
    render() {
        return (
            <div className='anamnesis-card'>
                <div className='anamnesis-card__header'>
                    <div className='anamnesis-card__date'>05/15/2018</div>
                    <i className='material-icons anamnesis-card__header__delete-i'>close</i>
                </div>

                <p className='anamnesis-card__p'>An individual may get diabetes when the pancreas can no longer secrete the needed hormones that produce insulin. The insulin maintains the glucose in the blood to be normal. </p>
            </div>
        );
    }
}

export default AnamnesisCard;
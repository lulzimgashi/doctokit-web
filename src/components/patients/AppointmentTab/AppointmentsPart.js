import React, { Component } from 'react';
import AppointmentRow from './AppointmentRow';

class AppointmentsPart extends Component {

    renderAppointments = () => {
        var els = [];
        for (let index = 0; index < 20; index++) {
            var el = <AppointmentRow />

            els.push(el);

        }

        return els;
    }

    render() {
        return (
            <div className='p-appoints-tab__appointments'>
                <div className='appointments-tab-header'>
                    <span className='appointments-tab-header__first'>Date and time</span>
                    <span>Description</span>
                </div>

                {this.renderAppointments()}
            </div>
        );
    }
}

export default AppointmentsPart;
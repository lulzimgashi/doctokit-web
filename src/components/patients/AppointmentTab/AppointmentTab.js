import React, { Component } from 'react';
import AppointmentsPart from './AppointmentsPart';
import AnamnesisPart from './AnamnesisPart';

class AppointmentTab extends Component {
    render() {
        return (
            <div style={{ width: '100%', maxWidth: '100%', overflowX: 'auto' }}>
                <div className='p-appoints-tab'>
                    <AppointmentsPart />
                    <AnamnesisPart />
                </div>
            </div>
        );
    }
}

export default AppointmentTab;
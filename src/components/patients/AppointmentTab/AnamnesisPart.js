import React, { Component } from 'react';
import DatePicker from '../../appointments/DatePicker';
import Input from '../../Input';
import Button from '../../Button';
import AnamnesisCard from './AnamnesisCard';

class AnamnesisPart extends Component {

    renderAnamnesis = () => {
        var els = [];

        for (let index = 0; index < 10; index++) {
            var card = <AnamnesisCard />

            els.push(card);
        }

        return els;
    }

    render() {
        return (
            <div className='p-appoints-tab__anamnesis'>
                <DatePicker
                    className='appointments-cal-w--no-shadow appointments-cal-w-no-paddings-l-r'
                    right
                    label='Date' />

                <Input
                    className='mrt-10px'
                    textarea
                    label
                    placeholder='Description' />

                <Button title='Add Anamnesis' className='dk-btn--medium-height mrt-20px' />
                {this.renderAnamnesis()}
            </div>
        );
    }
}

export default AnamnesisPart;
import React, { Component } from 'react';

class MessageBox extends Component {
    state = { messages: [1, 2], message: '' }

    renderMessages = () => {

        return this.state.messages.map((message, i) => {
            return (
                <span className='p-message-box__message'>{i} Canceled leservation</span>
            )
        })

    }

    onChange = () => {
        this.setState({ message: this.messageBox.value });
    }

    sendClicked = () => {
        var messages = this.state.messages;
        messages.push(this.messageBox.value);
        this.setState({ messages, message: '' });
    }

    _handleKeyPress = (e) => {
        if ((e.key === 'Enter') && this.messageBox.value.length > 0) {
            this.sendClicked();
        }
    }

    render() {

        return (
            <div className='p-boxes__item p-message-box'>
                <div className='p-message-box__title'>Send a message</div>

                <div className='p-message-box__messages-wrapper' ref={ref => this.messageWrapper = ref}>
                    {this.renderMessages()}
                </div>

                <div className='p-message-box__bottom'>
                    <input className='p-message-box__input' ref={ref => this.messageBox = ref} onChange={this.onChange} value={this.state.message} onKeyPress={this._handleKeyPress} placeholder='Write a message' />
                    <i className={'material-icons p-message-box__send-icon ' + (this.state.message.length > 0 ? 'p-message-box__send-icon--active' : '')} onClick={this.sendClicked} >send</i>
                </div>
            </div>
        );
    }
}

export default MessageBox;
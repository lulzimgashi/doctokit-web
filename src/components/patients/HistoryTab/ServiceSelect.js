import React, { Component } from 'react';

class ServiceSelect extends Component {
    state = { inputValue: '', error: false, popup: false, index: 0, item: {} }

    componentWillMount() {
        const selected = this.props.selected;
        this.setState({ items: this.props.items, item: this.props.items[selected], inputValue: this.props.items[selected][this.props.name], index: selected })
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleClickOutside = (event) => {
        if (this.state.popup && (this.wrapperRef && !this.wrapperRef.contains(event.target))) {
            this.setState({ popup: false })
        }
    }

    open = () => {
        if (!this.state.popup) {
            this.setState({ popup: true });
        }
    }

    chosen = (item, i) => {
        if (this.props.onChange) {
            this.props.onChange(item, i);
        }
        this.setState({ popup: false, index: i, item: item, inputValue: item[this.props.name] });
    }

    renderRows = () => {
        return this.state.items.map((item, i) => {
            return (
                <div key={i} className='drop-down__default-item' onClick={() => this.chosen(item, i)}>
                    <span>{item[this.props.name]}</span>
                </div>
            )
        })
    }


    render() {

        return (
            <div ref={divi => this.wrapperRef = divi} className={'appointments-cal-w appointments-header__item ' + (this.props.className || '')}>
                <span className='appointments-cal-w__label'>{this.props.label}</span>

                <div onClick={this.open} className='appointments-cal-w__last'>
                    <span className='appointments-cal-w__date'>{this.state.item[this.props.name]}</span>
                    <i className="material-icons drop-down-arrow-down">arrow_drop_down</i>
                </div>

                {this.state.popup && <div className='drop-down'>
                    {this.renderRows()}
                </div>}
            </div>
        );
    }
}


ServiceSelect.defaultProps = {
    selected: 0
}

export default ServiceSelect;
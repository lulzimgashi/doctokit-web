import React, { Component } from 'react';
import { connect } from 'react-redux'
//components
import DatePicker from '../../appointments/DatePicker';
import TimePickerP from '../TimePicker/TimePickerP';
import ServiceSelect from './ServiceSelect';
import Input from '../../Input';
import Button from '../../Button';

class HistoryTabFirst extends Component {
    state = { services: [] }

    componentWillMount() {
        var services = this.props.user.clinic.services;
        var newServices = [];

        for (var e in services) {
            if (services[e].active) {
                newServices.push({ name: services[e].name, value: services[e].id })
            }
        }

        this.setState({ services: newServices });
    }

    render() {
        const { services } = this.state;


        return (
            <div className='p-history-tab__first'>
                <div className='p-history-tab__selects-w'>
                    <DatePicker
                        className='appointments-cal-w--no-shadow'
                        label='Date' />
                    <TimePickerP
                        right
                        className='appointments-cal-w--no-shadow'
                        label='Time' />
                    <ServiceSelect
                        className='appointments-cal-w--no-shadow'
                        label='Services'
                        name='name'
                        items={services} />
                </div>

                <div className='p-history-tab__note-input-w'>
                    <Input
                        textarea
                        label
                        placeholder='Description'
                        className='mrl-15px mrt-20px width-100percent' />
                    <Button title='Add Note' className='mrl-50px' />
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return { user: state.user }
}

export default connect(mapStateToProps)(HistoryTabFirst);
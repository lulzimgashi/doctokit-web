import React, { Component } from 'react';
import ButtonPopup from '../../ButtonPopup';
import Button from '../../Button';


class HistoryNoteRow extends Component {
    state = { editMode: false, loading: false }

    switchEditMode = () => {
        this.setState({ editMode: !this.state.editMode });
    }

    save = () => {
        this.setState({ loading: true });
        setTimeout(() => {
            this.setState({ loading: false, editMode: false });
        }, 1000);
    }

    render() {
        var { editMode, loading } = this.state;

        return (
            <div className='notes-group__row'>
                <div className='notes-group__time-w'>
                    <span className='notes-group__time'>13:32</span>
                    <span className='notes-group__service'>Regular Control</span>
                </div>

                <div className={'notes-group__note ' + (editMode ? 'notes-group__note--active' : '')} contentEditable={editMode} >Joints are the areas where two or more bones meet. Most joints are mobile, allowing the bones to move. Joints consist of the following: Joints are the areas where two or more bones meet. Most joints are mobile, allowing the bones to move. Joints consist of the following: Joints are the areas where two or more bones meet. Most joints are mobile, allowing the bones to move. Joints consist of the following: Joints are the areas where two or more bones meet. Most joints are mobile, allowing the bones to move. Joints consist of the following:</div>

                {!editMode ?
                    <ButtonPopup icon='more_horiz' className='dk-btn--color-normal dk-btn--small-height dk-btn--border mrl-50px relative self-center '>
                        <div className={'drop-down '}>
                            <span className='drop-down__default-item' onClick={this.switchEditMode}>Edit</span>
                            <span className='drop-down__default-item' onClick={this.delete}>Delete</span>
                        </div>
                    </ButtonPopup> :

                    <div className='notes-group__action-w'>
                        <Button loading={loading} title='Save' click={this.save} className='dk-btn--small-height  open-sans dk-btn--w-70px mrb-10px' />
                        {!loading && <Button title='Cancel' click={this.switchEditMode} className='dk-btn--color-normal dk-btn--small-height dk-btn--border open-sans dk-btn--w-70px' />}
                    </div>}

            </div>
        );
    }
}

export default HistoryNoteRow;
import React, { Component } from 'react';
import { connect } from 'react-redux';
//components
import HistoryTabFirst from './HistoryTabFirst';
import HistoryNoteGroupRow from './HistoryNoteGroupRow';

class HistoryTab extends Component {
    state = { groups: [{ notes: [1, 2, 3, 4] }, { notes: [1, 2, 3, 4] }, { notes: [1, 2, 3, 4] }, { notes: [1, 2, 3, 4] }] }

    renderRows = () => {
        return this.state.groups.map((group, i) => {
            return <HistoryNoteGroupRow item={group} />
        })
    }

    render() {
        return (
            <div style={{ width: '100%', maxWidth: '100%', overflowX: 'auto' }}>
                <div className='p-history-tab'>
                    <HistoryTabFirst />

                    {this.renderRows()}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return { user: state.user }
}

export default connect(mapStateToProps)(HistoryTab);
import React, { Component } from 'react';
import HistoryNoteRow from './HistoryNoteRow';

class HistoryNoteGroupRow extends Component {

    state = { showReportsPopup: false }

    renderNotes = () => {
        return this.props.item.notes.map((note, i) => {
            return <HistoryNoteRow item={note} />
        })
    }

    switchReportsPopup = () => {
        if (!this.state.showReportsPopup) {
            this.setState({ showReportsPopup: true });
        }
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleClickOutside = (event) => {
        if (this.state.showReportsPopup && (this.wrapperRef && !this.wrapperRef.contains(event.target))) {
            this.setState({ showReportsPopup: false })
        }
    }

    render() {
        const { showReportsPopup } = this.state;

        return (
            <div className='notes-group' >
                <div className='notes-group__header'>
                    <i className='material-icons notes-group__icon'>event</i>
                    <span className='notes-group__date'>04 Jul 2018</span>

                    <div className='p-info-box__reports-dd-w' onClick={this.switchReportsPopup}>
                        <span>Report</span>
                        <i className='material-icons mrl-10px em-1-5'>assessment</i>
                        {showReportsPopup && <div ref={ref => this.wrapperRef = ref} className={'drop-down drop-down--width-auto '}>
                            <div className='drop-down__default-item' >Email <i className='material-icons em-1-2 mrl-10px'>email</i> </div>
                            <div className='drop-down__default-item' >Pdf <i className='material-icons em-1-2 mrl-10px'>picture_as_pdf</i> </div>
                        </div>}
                    </div>
                </div>

                {this.renderNotes()}
            </div>
        )
    }
}

export default HistoryNoteGroupRow;
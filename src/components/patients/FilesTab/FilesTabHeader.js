import React, { Component } from 'react';
import Button from '../../Button';

class FilesTabHeader extends Component {
    render() {
        return (
            <div className='p-files-tab__header'>
                <Button click={this.openAddPopup} title='Add Attachment' className='dk-btn--medium-height mrl-auto open-sans' />
            </div>
        );
    }
}

export default FilesTabHeader;
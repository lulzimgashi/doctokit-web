import React, { Component } from 'react';
import FilesTabHeader from './FilesTabHeader';
import Folder from './Folder';
import Files from './Files';

class FilesTab extends Component {

    state = { folder: null }

    renderFolders = () => {
        var els = [];

        for (let index = 0; index < 10; index++) {
            var el = <Folder openFolder={this.openFolder} key={index} />

            els.push(el);
        }

        return els;
    }

    openFolder = () => {
        this.setState({ folder: {} })
    }

    goBack = () => {
        this.setState({ folder: null })
    }

    render() {
        if (this.state.folder) {
            return <Files goBack={this.goBack} />
        }
        return (
            <div className='p-files-tab' >
                <FilesTabHeader />

                <div className='p-files-tab__folders'>
                    {this.renderFolders()}
                </div>

            </div>
        );
    }
}

export default FilesTab;
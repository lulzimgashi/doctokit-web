import React, { Component } from 'react';
import FilesTabHeader from './FilesTabHeader';
import File from './File';

class Files extends Component {

    renderFiles = () => {
        var els = [];

        for (let index = 0; index < 10; index++) {
            var el = <File key={index} />

            els.push(el);
        }

        return els;
    }

    render() {
        return (
            <div className='p-files-tab' >
                <div className='p-files-tab__header'>
                    <i onClick={this.props.goBack} class="material-icons p-files-tab__back-icon">keyboard_backspace</i>
                    <span className='p-files-tab__name'>Hand Condition</span>
                </div>

                <div className='p-files-tab__folders'>
                    {this.renderFiles()}
                </div>

            </div>
        );
    }
}

export default Files;
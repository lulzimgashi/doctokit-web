import React, { Component } from 'react';

class Folder extends Component {

    openFolder = () => {
        this.props.openFolder();
    }

    render() {
        return (
            <div className='p-files-tab__folder folder'>
                <img className='folder__thumb' src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRh191JMSEwvB6us8hVf0-CH-0a2CtbHN3JINsu3z89vIwJ0zdoCw' />

                <div className='folder__infos'>
                    <div className='folder__name-w'>
                        <span className='folder__name'>No Label</span>
                        <span className='folder__files-count'>2 Files</span>
                    </div>
                    <div onClick={this.openFolder} className='folder__more p-info-box__reports-dd-w color-primary folder__more--self-end'>
                        <span>More</span>
                        <i class="material-icons em-1-5">chevron_right</i>
                    </div>
                </div>

            </div>
        );
    }
}

export default Folder;
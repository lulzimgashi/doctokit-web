import React, { Component } from 'react';

class File extends Component {
    render() {
        return (
            <div style={{ backgroundImage: 'url(https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRh191JMSEwvB6us8hVf0-CH-0a2CtbHN3JINsu3z89vIwJ0zdoCw)' }} className='file'>
                <i class="material-icons file__delete-icon">delete</i>

                <div className='file__info-wrapper'>
                    <div className='file__info-bg' />
                    <span>Img123123.jpg</span>
                    <div onClick={this.openFolder} className='folder__more p-info-box__reports-dd-w color-primary'>
                        <span>More</span>
                        <i class="material-icons em-1-5">file_download</i>
                    </div>

                </div>
            </div>
        );
    }
}

export default File;
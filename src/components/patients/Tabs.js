import React, { Component } from 'react';
//components
import TabHeader from './TabHeader'
import HistoryTab from './HistoryTab/HistoryTab'
import AppointmentTab from './AppointmentTab/AppointmentTab';
import FilesTab from './FilesTab/FilesTab';

class Tabs extends Component {
    state = { tab: 2 }
    changeTab = (tab) => {
        this.setState({ tab });
    }

    render() {
        var { tab } = this.state;

        return (
            <div className='width-100percent'>
                <TabHeader changeTab={this.changeTab} />

                {(tab === 0) && <HistoryTab />}
                {(tab === 1) && <AppointmentTab />}
                {(tab === 2) && <FilesTab />}
            </div>
        );
    }
}

export default Tabs;
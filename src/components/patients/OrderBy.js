import React, { Component } from 'react';

class OrderBy extends Component {
    state = { popup: false, filter: { name: 'First Name', value: 'firstName' } }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleClickOutside = (event) => {

        if (this.state.popup && (this.wrapperRef && !this.wrapperRef.contains(event.target))) {
            this.setState({ popup: false })
        }
    }

    openPopup = () => {
        if (!this.state.popup) {
            this.setState({ popup: true });
        }
    }

    orderBy = (filter) => {
        var isSame = filter.value === this.state.filter.value;

        this.props.orderBy(filter.value, isSame);

        this.setState({ popup: false, filter: filter });
    }

    render() {
        const { filter } = this.state;


        return (
            <div className='patients-order-by'>
                <span className='patients-order-by__title'>ORDER BY</span>

                <div className='patients-order-by__options' ref={ref => this.wrapperRef = ref} onClick={this.openPopup}>
                    {filter.name} <i className='material-icons patients-order-by__arrow-down-i'>arrow_drop_down</i>
                    {this.state.popup && <div className='drop-down'>
                        <span className='drop-down__default-item' onClick={() => this.orderBy({ name: 'First Name', value: 'firstName' })}>First Name</span>
                        <span className='drop-down__default-item' onClick={() => this.orderBy({ name: 'Last Name', value: 'lastName' })}>Last Name</span>
                        <span className='drop-down__default-item' onClick={() => this.orderBy({ name: 'Gender', value: 'gender' })}>Gender</span>
                        <span className='drop-down__default-item' onClick={() => this.orderBy({ name: 'Birth Date', value: 'birthDate' })}>Birth Date</span>
                        <span className='drop-down__default-item' onClick={() => this.orderBy({ name: 'Phone', value: 'phone' })}>Phone</span>
                        <span className='drop-down__default-item' onClick={() => this.orderBy({ name: 'Email', value: 'email' })}>Email</span>
                        <span className='drop-down__default-item' onClick={() => this.orderBy({ name: 'Address', value: 'address' })}>Address</span>
                    </div>}
                </div>
            </div>
        );
    }
}

export default OrderBy;
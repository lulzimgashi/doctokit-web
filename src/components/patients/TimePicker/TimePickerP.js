import React, { Component } from 'react';
import Button from '../../Button';


class TimePickerP extends Component {
    state = { timePickerOn: false, time: { hour: '00', minutes: '00' } }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }


    handleClickOutside = (event) => {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {


            this.setState({ timePickerOn: false })
        }
    }

    switchTimePicker = () => {
        this.setState({ timePickerOn: !this.state.timePickerOn })
    }

    onChange = (type, value) => {
        var time = this.state.time;
        time[type] = value;
        this.setState({ time });
    }


    render() {
        var { timePickerOn, time } = this.state;

        var hour = time.hour;
        hour = hour.length === 1 ? '0' + hour : hour;

        var minutes = time.minutes;
        minutes = minutes.length === 1 ? '0' + minutes : minutes;

        return (
            <div ref={divi => this.wrapperRef = divi} className={'appointments-cal-w appointments-header__item ' + (this.props.className || '')}>
                <span className='appointments-cal-w__label'>{this.props.label}</span>

                <div onClick={this.switchTimePicker} className='appointments-cal-w__last'>
                    <span className='appointments-cal-w__date'>{hour + ':' + minutes}</span>
                    <i className="material-icons drop-down-arrow-down">arrow_drop_down</i>
                </div>

                {timePickerOn &&
                    <div ref={ref => this.main = ref} className={'drop-down time-picker ' + (this.props.right ? 'time-picker--right ' : '') + (this.props.timePopupClassName || '')}>
                        <div className='time-picker__first'>
                            <span className='time-picker__hour'>{hour}</span>
                            <span className='time-picker__h-m-seperator'>:</span>
                            <span className='time-picker__minutes'>{minutes}</span>
                        </div>

                        <div className='time-picker__second time-picker__second--margin-bottom-20'>
                            <input className='time-picker__input' type="range" min="0" max="23" onChange={e => this.onChange('hour', e.target.value)} />
                            <input className='time-picker__input mrt-25px' type="range" min="0" max="59" step="1" onChange={e => this.onChange('minutes', e.target.value)} />
                        </div>

                    </div>}
            </div>
        );
    }
}

export default TimePickerP;
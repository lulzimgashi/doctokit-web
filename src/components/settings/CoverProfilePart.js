import React, { Component } from 'react';
import { connect } from 'react-redux';
import { base64StringToBlob } from 'blob-util';
import { updateClinic, updateClinicImg } from '../../redux/actions';
//components
import Loader from '../Loader';

class CoverProfilePart extends Component {
    state = { editTitle: false, newCover: null, coverLoading: false, newProfile: null, profileLoading: false }


    componentWillMount() {
        this.setState({ clinic: this.props.user.clinic })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ clinic: nextProps.user.clinic })
    }

    
    componentWillUnmount() {
        this._mounted = false;
    }
    

    imgMode = '';
    choseImg = (mode) => {
        if (this.state.newCover && mode === 'cover') {
            this.setState({ coverLoading: true });

            var imgBlob = base64StringToBlob(this.state.newCover.replace('data:image/png;base64,', ''));

            this.props.dispatch(updateClinicImg(this.state.clinic, imgBlob, 'cover', (error, response) => {
                if (error) {
                    this.setState({ coverLoading: false })
                } else {
                    var clinic = this.state.clinic;
                    clinic.coverImage = response.data;
                    this.setState({ coverLoading: false, newCover: null });
                }
            }));

            return;
        }
        else if (this.state.newProfile && mode === 'profile') {
            this.setState({ profileLoading: true })

            var imgBlob = base64StringToBlob(this.state.newProfile.replace('data:image/png;base64,', ''));

            this.props.dispatch(updateClinicImg(this.state.clinic, imgBlob, 'profile', (error, response) => {
                if (error) {
                    this.setState({ profileLoading: false })
                } else {
                    var clinic = this.state.clinic;
                    clinic.profileImage = response.data;
                    this.setState({ profileLoading: false, newProfile: null });
                }
            }));


            return;
        }
        else {
            this.imgMode = mode;
            this.imgInput.click();
        }
    }

    fileChosen = (e) => {
        var fileReader = new FileReader();
        fileReader.onloadend = (e) => {
            var arrayBuffer = e.target.result;
            if (this.imgMode === 'cover') {
                this.setState({ newCover: arrayBuffer });
                this.imgInput.value = '';
                return;
            }
            if (this.imgMode === 'profile') {
                this.setState({ newProfile: arrayBuffer });
                this.imgInput.value = '';
                return;
            }

        }

        fileReader.readAsDataURL(this.imgInput.files[0]);
    }

    clearImage = (which) => {
        if (which === 'cover') {
            this.setState({ newCover: null });
            return;
        }

        if (which === 'profile') {
            this.setState({ newProfile: null });
        }
    }

    switchEditTitle = () => {
        
        if (this.state.editTitle) {
            var clinic = this.state.clinic;
            clinic.oldName = clinic.name;
            clinic.name = this.nameInput.value;

            this.props.dispatch(updateClinic(clinic, (error) => {
                if (error) {
                    clinic.name = clinic.oldName;
                    this.setState({ clinic });
                }
            }))
        }
        this.setState({ editTitle: !this.state.editTitle });;
    }

    _mounted=true;
    inputOnBlur=(e)=>{
        setTimeout(() => {
            if(this._mounted && this.state.editTitle){
            this.setState({ editTitle: !this.state.editTitle });
            }
        }, 200);
    }


    render() {
        const titleIcon = this.state.editTitle ? 'check' : 'edit';
        const clinic = this.state.clinic;

        const cover = this.state.newCover ? this.state.newCover : clinic.coverImage;
        const coverImgIcon = this.state.newCover ? 'check' : 'camera_alt';

        const profile = this.state.newProfile ? this.state.newProfile : clinic.profileImage;
        const profileImgIcon = this.state.newProfile ? 'check' : 'camera_alt';

        return (
            <div ref={ref => this.main = ref} className='s-cover' style={{ backgroundImage: 'url(' + cover + ')' }}>
            
                <input type="file" accept="image/jpg,image/jpeg,image/png" onChange={this.fileChosen} ref={ref => this.imgInput = ref} style={{ display: 'none' }} />
                {!this.state.coverLoading && <i className='s-cover__chose-img s-cover__chose-img--right material-icons' onClick={() => this.choseImg('cover')}>{coverImgIcon}</i>}
                {(this.state.newCover && !this.state.coverLoading) && <i className='s-cover__chose-img s-cover__chose-img--left material-icons' onClick={() => this.clearImage('cover')}>close</i>}
                {this.state.coverLoading && <div className='s-cover__chose-img s-cover__chose-img--right'><Loader color='#456AEB' /></div>}


                <div className='s-cover-bottom'>
                    <div className='relative'>
                        <div className='s-cover-bottom__profile-img' style={{ backgroundImage: 'url(' + profile + ')' }} />
                        
                        {!this.state.profileLoading && <i className='material-icons s-cover-bottom__profile-icon' onClick={() => this.choseImg('profile')}>{profileImgIcon}</i>}
                        {(this.state.newProfile && !this.state.profileLoading) && <i className='material-icons s-cover-bottom__profile-icon  s-cover-bottom__profile-icon--cancel' onClick={() => this.clearImage('profile')}>close</i>}
                        {this.state.profileLoading && <div className='s-cover-bottom__profile-icon'><Loader color='#456AEB' /></div>}
                    </div>

                    <div className='s-cover-bottom__name-w'>
                        <div className='s-cover-bottom__name-input'>
                            {!this.state.editTitle && <span>{clinic.name}</span>}
                            {this.state.editTitle && <input autoFocus ref={ref => this.nameInput = ref} defaultValue={clinic.name} className='s-cover-bottom__input' onBlur={this.inputOnBlur} />}
                            <i onClick={this.switchEditTitle} className='material-icons default-box-shadow em-0-7 mrl-10px pointer'>{titleIcon}</i>
                        </div>
                        <span className='open-sans color-white em-0-8'>Your account is {clinic.enabled ? 'active' : 'not active'}</span>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps=state=>{
    return {user:state.user};
}

export default  connect(mapStateToProps)(CoverProfilePart);
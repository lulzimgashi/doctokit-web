import React, { Component } from 'react';
//tabs
import InformationTab from './InformationTab';
import ScheduleTab from './ScheduleTab';

class ProfileTab extends Component {
    render() {
        return (
            <div style={{ display: this.props.active ? 'flex' : 'none' }} className='main-container__tabs mrt-25px'>
                <InformationTab />
                <ScheduleTab />
            </div>
        );
    }
}

export default ProfileTab;
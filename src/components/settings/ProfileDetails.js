import React, { Component } from 'react';
import { connect } from 'react-redux';
//components
import Input from '../Input';
import Button from '../Button';
import { updateClinic } from '../../redux/actions';

class ProfileDetails extends Component {
    state={loading:false}

    componentWillMount() {
        this.setState({ clinic: this.props.user.clinic })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ clinic: nextProps.user.clinic })
    }

    updateClicked = () => {
        if (this.state.loading || this.email.state.error) return;


        var newClinic = JSON.parse(JSON.stringify(this.state.clinic));
        newClinic.email=this.email.state.inputValue;
        newClinic.phone=this.phone.state.inputValue;
        newClinic.address=this.address.state.inputValue;
        newClinic.website=this.website.state.inputValue;
        newClinic.description=this.description.state.inputValue;

        var oldClinic=this.state.clinic;

        if (JSON.stringify(oldClinic) !== JSON.stringify(newClinic)) {
            this.setState({ loading: true, clinic: newClinic });
            this.props.dispatch(updateClinic(newClinic,(error)=>{
                if(error){
                    this.setState({ loading: false, clinic: oldClinic });
                }else{
                    this.setState({ loading: false });
                }
            }));
        }

    }

    render() {
        const clinic = this.state.clinic;

        return (
            <div className='s-profile-details'>
                <span className='s-profile-details__title'>Profile Details</span>

                <div className='s-profile-details__row'>
                    <Input ref={ref => this.email = ref} label placeholder='Email' type='email' className='width-100percent' value={clinic.email}/>
                    <Input ref={ref => this.phone = ref} label placeholder='Phone' type='text' className='width-100percent mrl-50px' value={clinic.phone}/>
                </div>

                <div className='s-profile-details__row'>
                    <Input ref={ref => this.address = ref} label placeholder='Address' type='text' className='width-100percent' value={clinic.address} />
                    <Input ref={ref => this.website = ref} label placeholder='Website' type='text' className='width-100percent mrl-50px' value={clinic.website} />
                </div>

                 <div className='s-profile-details__row'>
                    <Input textarea ref={ref => this.description = ref} label placeholder='Description' type='text' className='width-100percent' value={clinic.description} />
                </div>


                <div className='s-profile-details__row s-profile-details__row--justify-end'>
                    <Button title='Update' loading={this.state.loading} click={this.updateClicked} />
                </div>

            </div>
        );
    }
}

const mapStateToProps=state=>{
    return {user:state.user};
}

export default connect(mapStateToProps)(ProfileDetails);
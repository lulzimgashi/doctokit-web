import React, { Component } from 'react';
import CoverProfilePart from './CoverProfilePart';
import ProfileDetails from './ProfileDetails';

class MainInformationTab extends Component {

    render() {
        return (
            <div className='main-container__tab mrr-30px'>
                  <CoverProfilePart user={this.props.user}/>
                  <ProfileDetails user={this.props.user}/>
            </div>
        );
    }
}


export default MainInformationTab;
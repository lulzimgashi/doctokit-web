import React, { Component } from 'react';
import { connect } from 'react-redux'
//components
import Button from '../Button';
import Select from '../Select';
import Switch from '../Switch';
import LocationAutoSuggest from '../LocationAutoSuggest';
import { updateClinic } from '../../redux/actions';

const slotTimes = [
    { name: '10 Minutes', value: 10 },
    { name: '15 Minutes', value: 15 },
    { name: '20 Minutes', value: 20 },
    { name: '25 Minutes', value: 25 },
    { name: '30 Minutes', value: 30 },
    { name: '35 Minutes', value: 35 },
    { name: '40 Minutes', value: 40 },
    { name: '45 Minutes', value: 45 },
    { name: '50 Minutes', value: 50 },
    { name: '55 Minutes', value: 55 },
    { name: '60 Minutes', value: 60 }

]

class OptionsTab extends Component {
    state = { loading: false, clinic: {} }


    componentWillMount() {
        this.setState({ clinic: this.props.user.clinic })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ clinic: nextProps.user.clinic })
    }


    updateClicked = () => {
        if (this.state.loading) return;

        var newClinic = JSON.parse(JSON.stringify(this.state.clinic));
        newClinic.language = this.selectLanguage.state.item.value;
        newClinic.slotTime = this.selectSlotTime.state.item.value;
        newClinic.longitude = this.location.state.chosenPlace.geometry.location.lng;
        newClinic.latitude = this.location.state.chosenPlace.geometry.location.ltd;
        newClinic.location = this.location.state.chosenPlace.formatted_address;
        newClinic.sms = this.sms.state.checked;

        var oldClinic = this.state.clinic;

        if (JSON.stringify(oldClinic) !== JSON.stringify(newClinic)) {
            this.setState({ loading: true, clinic: newClinic });
            this.props.dispatch(updateClinic(newClinic, (error) => {
                if (error) {
                    this.setState({ loading: false, clinic: oldClinic });
                } else {
                    this.setState({ loading: false });
                }
            }));
        }

    }

    timeSlotSelected = () => {
        const clinic = this.state.clinic;

        for (var e in slotTimes) {
            if (slotTimes[e].value === clinic.slotTime) {
                return e;
            }
        }
    }

    render() {
        const clinic = this.state.clinic;

        return (
            <div style={{ display: this.props.active ? 'block' : 'none' }} className='s-profile-details s-options'>
                <span className='s-profile-details__title'>Profile Details</span>

                <div className='s-profile-details__row'>
                    <Select ref={ref => this.selectLanguage = ref}
                        className='width-100percent'
                        label
                        placeholder='Language'
                        items={[{ name: 'Albanian', value: 'al' }, { name: 'English', value: 'en' }]}
                        selected={clinic.language === 'en' ? 1 : 0}
                        name='name' />

                    <Select ref={ref => this.selectSlotTime = ref}
                        className='width-100percent  mrl-50px'
                        label
                        placeholder='Slot Time'
                        items={slotTimes}
                        selected={this.timeSlotSelected()}
                        name='name' />
                </div>

                <div className='s-profile-details__row'>
                    <LocationAutoSuggest ref={ref => this.location = ref} label placeholder='Location' className='width-100percent' location={{ lng: clinic.longitude, ltd: clinic.latitude, locationName: clinic.location }} />
                    <Switch ref={ref => this.sms = ref} label placeholder='SMS Enabled' type='text' className='width-100percent mrl-50px' value={clinic.sms} />
                </div>


                <div className='s-profile-details__row s-profile-details__row--justify-end'>
                    <Button title='Update' loading={this.state.loading} click={this.updateClicked} />
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return { user: state.user };
}

export default connect(mapStateToProps)(OptionsTab);
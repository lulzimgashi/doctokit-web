import React, { Component } from 'react';
//compoents
import Loader from '../Loader';
import TimePicker from './TimePicker';
//actions
import { updateDay, CLINIC_UPDATED } from '../../redux/actions';

class ScheduleRow extends Component {
    state = { loading: false, item: { start: '09:00', end: '17:00', open: true } }

    
    componentWillMount() {
        this.setState({item:this.props.day})
    }
    


    addOrRemove = () => {
        if (this.state.loading) {
            return;
        }

        var item = this.state.item;
        var newItem = JSON.parse(JSON.stringify(item));
        newItem.open = !item.open;

        this.props.dispatch(updateDay(newItem, (error) => {
            if (error) {
                this.setState({ loading: false });
            } else {
                item = newItem;
                this.props.days[this.props.index]=item;
                this.setState({ item });
            }
        }))
    }

    save = (type, value) => {
        this.setState({ loading: true });

        var item = this.state.item;
        var newItem = JSON.parse(JSON.stringify(item));
        newItem[type] = value;

        this.props.dispatch(updateDay(newItem, (error) => {
            if (error) {
                this.setState({ loading: false });
            } else {
                item = newItem;
                this.props.days[this.props.index]=item;
                this.setState({ item ,loading:false});
            }
        }))
    }

    getTitle = (day) => {
        switch (day) {
            case 1:
                return 'Monday';
            case 2:
                return 'Tuesday';
            case 3:
                return 'Wednesday';
            case 4:
                return 'Thursday';
            case 5:
                return 'Friday';
            case 6:
                return 'Saturday';
            case 7:
                return 'Sunday';
            default:
                return 'Monday';
        }
    }

    render() {
        const icon = this.state.changed ? 'check' : this.state.item.open ? 'remove' : 'add';

        return (
            <div className={'schedule-row ' + (this.props.className)}>
                <span className='scheduler-row__title'>{this.getTitle(this.props.day.day)}</span>

                <div className='schedule-row__item mrt-7px'>
                    {icon === 'add' ?
                        <span className='schedule-row__no-hours'>No Working Hours</span>
                        : <div className='schedule-row__times-wrapper'>
                          
                            <div className='time-wrapper'>
                                <i className='material-icons time-wrapper__icon'>access_time</i>
                                <TimePicker
                                    time={this.state.item.start}
                                    save={(value) => this.save('start', value)}/>
                            </div>

                            <i className='material-icons schedule-row__times-wrapper-icon'>keyboard_backspace</i>

                            <div className='time-wrapper'>
                                <i className='material-icons time-wrapper__icon'>access_time</i>
                                <TimePicker
                                    time={this.state.item.end}
                                    save={(value) => this.save('end', value)}
                                    right />
                            </div>

                        </div>}

                    {this.state.loading ?
                        <Loader className='schedule-row__loader' color='#456AEB' />
                        : <i onClick={this.addOrRemove} className='material-icons schedule-row__button-icon'>{icon}</i>}
                </div>
            </div>
        );
    }
}

export default ScheduleRow;
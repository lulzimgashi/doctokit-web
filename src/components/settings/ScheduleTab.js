import React, { Component } from 'react';
import { connect } from 'react-redux';
//components
import ScheduleRow from './ScheduleRow';
class ScheduleTab extends Component {

    renderDayRows=()=>{
        return this.props.user.clinic.days.map((day,i)=>{
            return <ScheduleRow key={i} day={day} index={i} className='mrt-40px' dispatch={this.props.dispatch} days={this.props.user.clinic.days}/>
        })
    }

    render() {
        return (
            <div className='main-container__tab main-container__tab--schedule-tab'>
                <h3 className='font-book color-grey'>Working Hours</h3>
                <span className='default-p color-light-grey em-0-8 mrt-10px'>Please select the working hours for your clinic.</span>

                {this.renderDayRows()}
            </div>
        );
    }
}

const mapStateToProps=state=>{
    return {user:state.user}
}

export default connect(mapStateToProps)(ScheduleTab);
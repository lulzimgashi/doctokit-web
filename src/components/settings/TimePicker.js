import React, { Component } from 'react';
import Button from '../Button';

class TimePicker extends Component {
    state = { time: { hour: '00', minutes: '00' }, popup: false }


    componentWillMount() {
        const time = this.props.time.split(':');
        var timeObj = { hour: time[0], minutes: time[1] }

        this.setState({ time: timeObj })
    }

    componentWillReceiveProps(nextProps) {
        const time = nextProps.time.split(':');
        var timeObj = { hour: time[0], minutes: time[1] }

        this.setState({ time: timeObj })
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    onChange = (type, value) => {
        var time = this.state.time;
        time[type] = value;
        this.setState({ time });
    }

    open = () => {
        if (!this.state.popup) {
            this.setState({ popup: true });
        }
    }

    handleClickOutside = (event) => {
        if (this.main && !this.main.contains(event.target)) {

            const time = this.props.time.split(':');
            var timeObj = { hour: time[0], minutes: time[1] }

            this.setState({ popup: false, time: timeObj })
        }
    }

    save = () => {
        var hour = this.state.time.hour;
        hour = hour.length === 1 ? '0' + hour : hour;

        var minutes = this.state.time.minutes;
        minutes = minutes.length === 1 ? '0' + minutes : minutes;

        var time = hour + ':' + minutes;

        this.props.save(time);
        this.setState({ popup: false })
    }

    shouldComponentUpdate(nextProps, nextState) {


        if ((this.props.time !== nextProps.time) || (this.state !== nextState)) {
            return true;
        }


        return false;
    }


    render() {
        var hour = this.state.time.hour;
        hour = hour.length === 1 ? '0' + hour : hour;

        var minutes = this.state.time.minutes;
        minutes = minutes.length === 1 ? '0' + minutes : minutes;

        return (
            <span onClick={this.open} className={'time-wrapper__hour ' + (this.props.className || '')}>
                {hour + ':' + minutes}
                {this.state.popup && <div ref={ref => this.main = ref} className={'drop-down time-picker ' + (this.props.right ? 'time-picker--right ' : '') + (this.props.timePopupClassName || '')}>
                    <div className='time-picker__first'>
                        <span className='time-picker__hour'>{hour}</span>
                        <span className='time-picker__h-m-seperator'>:</span>
                        <span className='time-picker__minutes'>{minutes}</span>
                    </div>

                    <div className='time-picker__second'>
                        <input className='time-picker__input' type="range" min="0" max="23" onChange={e => this.onChange('hour', e.target.value)} />
                        <input className='time-picker__input mrt-25px' type="range" min="0" max="59" step="10" onChange={e => this.onChange('minutes', e.target.value)} />
                    </div>

                    <Button title='Save' click={this.save} className='dk-btn--small-height mrt-25px' />
                </div>}
            </span>
        );
    }
}

export default TimePicker;
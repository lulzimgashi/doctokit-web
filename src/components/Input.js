import React, { Component } from 'react';
import { validateEmail, validateIsNumber } from '../index';

class Input extends Component {
    state = { focused: false, inputValue: '', error: false }


    componentWillMount() {
        if (this.props.value) {
            this.setState({ inputValue: this.props.value.toString() })
        }
    }


    onFocus = () => {
        this.setState({ focused: true });
    }
    onBlur = () => {
        this.setState({ focused: false, inputValue: this.input.value });
    }
    onChange = (e) => {
        if (this.props.type === 'email') {
            if (this.input.value.length > 0 && !validateEmail(this.input.value)) {
                this.setState({ error: true });
                return;
            } else if (this.state.error) {
                this.setState({ error: false });
            }
        }

        if (this.props.type === 'phone') {
            if (this.input.value.length > 0 && !validateIsNumber(this.input.value)) {
                this.setState({ error: true });
                return;
            } else if (this.state.error) {
                this.setState({ error: false });
            }
        }

        if (this.props.maxLength) {
            if (this.input.value.length > this.props.maxLength) {
                this.setState({ error: true });
                return;
            } else if (this.state.error) {
                this.setState({ error: false });
            }
        }
    }
    render() {
        const error = this.props.error || this.state.error ? ' error' : '';
        return (
            <div className={'dk-input-w ' + (this.props.className || '')}>
                {this.props.textarea ?
                    <textarea type={this.props.type} ref={input => this.input = input} onChange={this.onChange} className={'dk-input dk-input--text-area ' + (error)} onFocus={this.onFocus} onBlur={this.onBlur} defaultValue={this.props.value} />
                    : <input type={this.props.type} ref={input => this.input = input} onChange={this.onChange} className={'dk-input ' + (error)} onFocus={this.onFocus} onBlur={this.onBlur} defaultValue={this.props.value} />}
                
                <div className={'dk-input-w__border ' + (error)}></div>
                {this.props.label && <label className={'dk-input-w__label' + (error) + (this.state.focused || this.state.inputValue.length > 0 ? ' dk-input-w__label--on' : '')}>{this.props.placeholder}</label>}
            </div>
        );
    }
}

export default Input;


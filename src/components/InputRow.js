import React, { Component } from 'react';


class Input extends Component {
    state = { focused: false, inputValue: '', error: false }
    onFocus = () => {
        this.setState({ focused: true });
    }
    onBlur = () => {
        this.setState({ focused: false, inputValue: this.input.value });
    }

    onChange = (e) => {
        if (e.target.value.length === 0 && !this.state.error) {
            this.setState({ error: true });
        } else if (this.state.error) {
            this.setState({ error: false });
        }
    }

    render() {
        const error = this.props.error || this.state.error ? ' error' : '';
        return (
            <div className={'dk-input-w ' + this.props.className}>
                <input readOnly={this.props.readOnly} autoFocus onChange={this.onChange} ref={input => this.input = input} className={'dk-input '} onFocus={this.onFocus} onBlur={this.onBlur} defaultValue={this.props.value} />
                <div className={'dk-input-w__border' + (error)}></div>
            </div>
        );
    }
}

export default Input;